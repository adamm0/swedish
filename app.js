'use strict';
const exec = require('child_process').exec;
const secure = require('express-force-https');
const compression = require('compression');
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const exphbs = require('express-handlebars');
const swedish = require('./public/OUTPUT.json');
const fs = require('fs');
const _ = require('lodash');
const mongoose = require('mongoose');
const browserify = require('browserify');
const basicAuth = require('basic-auth');
const colors = require('colors');

const auth = function (req, res, next) {
  function unauthorized(res) {
    res.set('WWW-Authenticate', 'Basic realm=Authorization Required');
    return res.send(401);
  }

  const user = basicAuth(req);

  if (!user || !user.name || !user.pass) {
    return unauthorized(res);
  }

  if (user.name === 'lynn' && user.pass === 'password') {
    return next();
  } else {
    return unauthorized(res);
  }
};

const PersonSchema = new mongoose.Schema({
  id: String,
  nameFirst: String,
  nameLast: String,
  address: String,
  familiesBornInto: Array,
  familiesStarted: Array,
  phone: String,
  events: Array,
  noteRefs: Array
});

const FamilySchema = new mongoose.Schema({
  id: String,
  husbandId: String,
  wifeId: String,
  childIds: Array,
  events: Array
});

const NoteSchema = new mongoose.Schema({
  id: String,
  value: String,
  sourceCitationsUnderValue: Boolean
});

const SourceSchema = new mongoose.Schema({
  id: String,
  title: String,
  repo: Array
});

const PersonLog = mongoose.model('PersonLog', PersonSchema);
const FamilyLog = mongoose.model('FamilyLog', FamilySchema);
const NoteLog = mongoose.model('NoteLog', NoteSchema);
const SourceLog = mongoose.model('SourceLog', SourceSchema);

const PersonChangeLog = mongoose.model('PersonLog', PersonSchema, 'changes');
const FamilyChangeLog = mongoose.model('FamilyLog', FamilySchema, 'changes');
const NoteChangeLog = mongoose.model('NoteLog', NoteSchema, 'changes');
const SourceChangeLog = mongoose.model('SourceLog', SourceSchema, 'changes');

mongoose.connect('mongodb://localhost/testdb');
//mongoose.connect('mongodb://test:Test5378@ds031995.mlab.com:31995/swedish');

/*

PersonLog.remove().exec();
FamilyLog.remove().exec();
NoteLog.remove().exec();
SourceLog.remove().exec();

const getByTag = function(obj, str) {
  return _.reduce(_.filter(obj, (item) => { 
      return item.tag === str; })[0], (prev, curr) => {
        typeof curr === 'object' ? curr = '' : null;
        return prev + curr + '<br>';
    }, '');
};

swedish.people.forEach( (person) => {
  var entry = new PersonLog({ 
    id: person.id,
    phone: person.phon,
    familiesBornInto: person.famc,
    familiesStarted: person.fams,
    address: _.reduce(person.addr, (prev, curr) => { return prev + curr + '<br>'; }),
    nameFirst: person.names[0].value.slice(0, person.names[0].value.indexOf('/')),
    nameLast: person.names[0].value.slice(person.names[0].value.indexOf('/') + 1, person.names[0].value.length - 1),
    events: person.eventsFacts,
    noteRefs: person.noteRefs
  });
  entry.save(function(err) {
    if (err) {
      console.log(err);
    } else {
      console.log(">>>>>>>", entry);
    }
  });
});

swedish.families.forEach( (family) => {
  console.log("^^^^^^^^^^",family);
  const entry = new FamilyLog({
    id: family.id,
    husbandId: family.husbandRefs ? family.husbandRefs[0].ref : null,
    wifeId: (family.wifeRefs === undefined) ? 'undefined' : family.wifeRefs[0].ref,
    childIds: family.childRefs,
    events: family.eventsFacts
  });
  entry.save(function(err) {
    if(err){
      console.log(err);
    } else {
      console.log("$$$$ $$$$ $$$$", entry);
    }
  });
});


swedish.notes.forEach( (note) => {


  const entry = new NoteLog({
    id: note.id,
    value: note.value,
    sourceCitationsUnderValue: note.sourceCitationsUnderValue
  });

  entry.save(function(err) {
    if(err){
      console.log(err);
    } else {
      console.log("$(((((((((((($$$ $$$$ $$$$))))))))))))", entry);
    }
  });

});


swedish.sources.forEach( (source) => {

  const entry = new SourceLog({
    id: source.id,
    value: source.value,
    title: source.titl,
    repo: source.repo
  });

  entry.save(function(err) {
    if(err){
      console.log(err);
    } else {
      console.log("$$$$ $$$$ $$$$", entry);
    }
  });

});



function ObjToSource(o){
   if (!o) return 'null';
   if (typeof(o) == "object") {
       if (!ObjToSource.check) ObjToSource.check = new Array();
       for (var i=0, k=ObjToSource.check.length ; i<k ; ++i) {
           if (ObjToSource.check[i] == o) {return '{}';}
       }
       ObjToSource.check.push(o);
   }
   var k="",na=typeof(o.length)=="undefined"?1:0,str="";
   for(var p in o){
       if (na) k = "'"+p+ "':";
       if (typeof o[p] == "string") str += k + "'" + o[p]+"',";
       else if (typeof o[p] == "object") str += k + ObjToSource(o[p])+",";
       else str += k + o[p] + ",";
   }
   if (typeof(o) == "object") ObjToSource.check.pop();
   if (na) return "{"+str.slice(0,-1)+"}";
   else return "["+str.slice(0,-1)+"]";
}

*/

browserify('./src/test.js')
  .transform('babelify', {presets: ['es2015']})
  .bundle()
  .pipe(fs.createWriteStream('./public/js/bundle-test.js'));

browserify('./src/testmod.js')
  .transform('babelify', {presets: ['es2015']})
  .bundle()
  .pipe(fs.createWriteStream('./public/js/bundle-testmod.js'));

browserify('./src/admin_bak.js')
  .transform('babelify', {presets: ['es2015']})
  .bundle()
  .pipe(fs.createWriteStream('./public/js/bundle.js'));

browserify('./src/login.js')
  .transform('babelify', {presets: ['es2015']})
  .bundle()
  .pipe(fs.createWriteStream('./public/js/bundle-login.js'));

browserify('./src/secure.js')
  .transform('babelify', {presets: ['es2015']})
  .bundle()
  .pipe(fs.createWriteStream('./public/js/bundle-secure.js'));

browserify('./src/material.js')
  .transform('babelify', {presets: ['es2015']})
  .bundle()
  .pipe(fs.createWriteStream('./public/js/bundle-material.js'));

browserify('./src/modify.js')
  .transform('babelify', {presets: ['es2015']})
  .bundle()
  .pipe(fs.createWriteStream('./public/js/bundle-modify.js'));

browserify('./src/admin.js')
  .transform('babelify', {presets: ['es2015']})
  .bundle()
  .pipe(fs.createWriteStream('./public/js/bundle-admin.js'));

browserify('./src/tree.js')
  .transform('babelify', {presets: ['es2015']})
  .bundle()
  .pipe(fs.createWriteStream('./public/js/bundle-tree.js'));

app.use(compression({ level: 9 }));

//app.use(secure);

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.set('view engine', 'handlebars');

app.set('port', (process.env.PORT || 5000));

app.use(express.static(__dirname + '/public'));

app.engine('handlebars', exphbs({
  defaultLayout: 'main', 
  helpers: {
    toJSON : function(object) {
      return JSON.stringify(object);
    }
  }
}));

// API GET routes

app.get('/api/getdatabackups', (req, res) => {
  const cmd = 'ls backup';
  exec(cmd, function(error, stdout, stderr) {
    const output = stdout.split('\n');
    res.json(output);
  });
});

app.get('/api/changes', (req, res) => {
  NoteChangeLog.find({}, (err, data) => {
  if (err) {
      console.log(err);
    } else {
      res.json(data);
    }
  });
});

app.get('/api/all', (req, res) => { 
  const json = {};

  FamilyLog.find({}, (err, data) => {
    if (err) {
      console.log(err);
      res.send(err);
    } else {
      json.families = (data);
      PersonLog.find({}, (err, data) => {
        if (err) {
          console.log(err);
          res.send(err);
        } else {
          json.people = data; 
          //res.json(json);
          SourceLog.find({}, (err, data) => {
            if (err) {
              console.log(err);
              res.send(err);
            } else {
              json.sources = data;
              res.json(json);
            }
          });
        }
      });
    }
  });

});


app.get('/api/families', (req, res) => { 
  FamilyLog.find({}, (err, data) => {
    if (err) {
      console.log(err);
    } else {
      res.json(data);
    }
  });
});

app.get('/api/family', (req, res) => {
  FamilyLog.find({ id: req.query.id }, (err, data) => {
    if (err) {
      console.log(err);
    } else {
      res.json(data);
    }
  });
});

app.get('/api/familySearchHusband', (req, res) => {
  if (req.query.husbandId !== undefined) {
    FamilyLog.find({ husbandId: req.query.husbandId }, (err, data) => {
      if (err) {
        console.log(err);
      } else {
        res.json(data);
      }
    });
  }
});

app.get('/api/people', (req, res) => {
  PersonLog.find({}, function(err, data) {
    if (err) {
      console.log(err);
    } else {
      res.json(data);
    }
  });
});

app.get('/api/person', (req, res) => {
  PersonLog.find({ id: req.query.id }, function(err, data) {
    if (err) {
      console.log(err);
    } else {
      res.json(data);
    }
  });
});

app.get('/api/notes', (req, res) => {
  NoteLog.find({}, (err, data) => {
    if (err) { 
      console.log(err); 
    } else {
      res.json(data);
    }
  });
});

app.get('/api/note', (req, res) => {
  NoteLog.find({ id: req.query.id }, (err, data) => {
    if (err) { 
      console.log(err); 
    } else {
      res.json(data);
    }
  });
});

app.get('/api/sources', (req, res) => {
  SourceLog.find({}, (err, data) => {
    if (err) { 
      console.log(err); 
    } else {
      res.json(data);
    }
  });
});

app.get('/api/source', (req, res) => {
  SourceLog.find({ id: id }, (err, data) => {
    if (err) { 
      console.log(err); 
    } else {
      res.json(data);
    }
  });
});

// API POST routes

app.post('/api/restoredatabackups', (req, res) => {
  const cmd = './db-restore.sh ' + "'" + req.body.timestamp + "'";
  exec(cmd, function(error, stdout, stderr) {
    console.log(stdout, stderr);
    res.json(stdout, stderr);
    res.end('200');
  });
});

app.post('/api/createdatabackup', (req, res) => {
  const cmd = './db-backup.sh';
  exec(cmd, function (err, stdout, stderr) {
    console.log(stdout, stderr);
    //res.json(stdout, stderr);
    res.end('200');
  })
});



app.post('/api/person', (req, res) => {
  console.log('oh my');
  const person = req.body.person;
  PersonLog.findOneAndUpdate({ id: person.id }, person, function(err, person) {
    if (err) {
      console.log('error');
      res.end('error');
    }
  });
  res.end('success');
});

app.post('/api/person/property', (req, res) => {
  const person = req.body.person;
  const id = person.id;
  const changed = req.body.title;
  let update = {};

  update[changed] = person[changed];

  PersonLog.findOneAndUpdate({ id: id }, update, function(err, person) {
    if (err) {
      console.log('got an error');
    }
  });
  res.end('err!!');
});

app.post('/api/person/savepending', (req, res) => {

  const entry = new PersonChangeLog({
    id: req.body.id,
    value: req.body.value,
    repo: req.body.repo
  });

  entry.save(function(err) {
    if(err) {
      console.log(err);
    } else {
      console.log("$$$$ $$$$ $$$$", entry);
    }
  });

});

// Views

app.get('/testmod', auth, (req, res) => {
  res.render('testmod');
});

app.get('/testagain', auth, function(req, res) {
  res.sendFile(__dirname + '/views/testagain.html');
});

app.get('/', (req, res) => {
  res.render('index');
  console.log('sent login');
});

app.get('/login', (req, res) => {
  res.render('login');
  console.log('sent login');
});

app.get('/secure', auth, (req, res) => {
  res.render('secure');
});

app.get('/modify', function (req, res) {
  const familyId = req.query.id;
if(familyId !== 'undefined' && familyId) {
  FamilyLog.find({ id: familyId }, (err, data) => { 
    if (err) {
      console.log(err);
    } else {
      console.log(data[0]);
      const wifeId = data[0].wifeId;
      const husbandId = data[0].husbandId;

      PersonLog.findOne({ id: husbandId }, function (err, husbandData) {
        if (err) {
          console.log(err);
        } else {
          console.log("!",husbandData);

          PersonLog.findOne({ id: wifeId }, function (err, wifeData) {
            if (err) {
              console.log(err);
            } else {
              husbandData.toObject();
              const vars = {
                marriageDate: _.filter(data[0].events, event => { return event.tag === 'MARR'; })[0] ? _.filter(data[0].events, event => { return event.tag === 'MARR'; })[0].date : 'undefined',
                marriagePlace: _.filter(data[0].events, event => { return event.tag === 'MARR'; })[0] ? _.filter(data[0].events, event => { return event.tag === 'MARR'; })[0].place : 'undefined',
                children: data[0].children,

                husbandName: husbandData.nameFirst + husbandData.nameLast,
                husbandBirthDate: _.filter(husbandData.events, event => { return event.tag === 'BIRT'; })[0] ? _.filter(husbandData.events, event => { return event.tag === 'BIRT'; })[0].date : 'undefined',
                husbandBirthPlace: _.filter(husbandData.events, event => { return event.tag === 'BIRT'; })[0] ? _.filter(husbandData.events, event => { return event.tag === 'BIRT'; })[0].place : 'undefined',
                husbandDeathDate: _.filter(husbandData.events, event => { return event.tag === 'DEAT'; })[0] ? _.filter(husbandData.events, event => { return event.tag === 'DEAT'; })[0].date : 'undefined',
                husbandDeathPlace: _.filter(husbandData.events, event => { return event.tag === 'DEAT'; })[0] ? _.filter(husbandData.events, event => { return event.tag === 'DEAT'; })[0].place : 'undefined',
                husbandFamilyBornInto: husbandData.familiesBornIntoID[0] ? husbandData.familiesBornIntoID[0].ref : 'undefined',

                wifeName: wifeData.nameFirst + wifeData.nameLast,
                wifeBirthDate: _.filter(wifeData.events, event => { return event.tag === 'BIRT'; })[0] ? _.filter(wifeData.events, event => { return event.tag === 'BIRT'; })[0].date : 'undefined',
                wifeBirthPlace: _.filter(wifeData.events, event => { return event.tag === 'BIRT'; })[0] ? _.filter(wifeData.events, event => { return event.tag === 'BIRT'; })[0].place : 'undefined',
                wifeDeathDate: _.filter(wifeData.events, event => { return event.tag === 'DEAT'; })[0] ? _.filter(wifeData.events, event => { return event.tag === 'DEAT'; })[0].date : 'undefined',
                wifeDeathPlace: _.filter(wifeData.events, event => { return event.tag === 'DEAT'; })[0] ? _.filter(wifeData.events, event => { return event.tag === 'DEAT'; })[0].place : 'undefined',
                wifeParents: undefined,
                wifeFamilyBornInto: wifeData.familiesBornIntoID[0] ? wifeData.familiesBornIntoID[0].ref : 'undefined'
              };
              res.render('modify', vars);
            }
          });
        }
      });
    }
  }); 
} else {
    res.render('modify', {
                marriageDate: 'undefined',
                marriagePlace: 'undefined',
                children: 'undefined',

                husbandName: 'undefined',
                husbandBirthDate: 'undefined',
                husbandBirthPlace: 'undefined',
                husbandDeathDate: 'undefined',
                husbandDeathPlace: 'undefined',
                husbandFamilyBornInto: 'undefined',

                wifeName: 'undefined',
                wifeBirthDate: 'undefined',
                wifeBirthPlace: 'undefined',
                wifeDeathDate: 'undefined',
                wifeDeathPlace: 'undefined',
                wifeParents: 'undefined',
                wifeFamilyBornInto: 'undefined'
              });
  }
  

/*
  PersonLog.find({ id: id }, (err, data) => {
    if (err) {
      console.log(err);
    } else {
      console.log(data);

      let vars = {
        husbandName: data[0].nameFirst + data[0].nameLast,
        husbandBirthDate: _.filter(data[0].events, el => { return el.tag === 'BIRT'; })[0].date,
        husbandBirthPlace: _.filter(data[0].events, el => { return el.tag === 'BIRT'; })[0].place,
        husbandDeathDate: _.filter(data[0].events, el => { return el.tag === 'DEAT'; })[0].date,
        husbandDeathPlace: _.filter(data[0].events, el => { return el.tag === 'DEAT'; })[0].place
      };

      PersonLog.find({ id: wifeId }, (err, data) => {
        if(err){
          console.log(err);
        } else {
          vars.wifeName = data[0].nameFirst + data[0].nameLast;
          vars.wifeBirthDate = _.filter(data[0].events, el => { return el.tag === 'BIRT'; })[0].date;
          vars.wifeBirthPlace = _.filter(data[0].events, el => { return el.tag === 'BIRT'; })[0].place;
          vars.wifeDeathDate = _.filter(data[0].events, el => { return el.tag === 'DEAT'; })[0].date;    
          vars.wifeDeathPlace = _.filter(data[0].events, el => { return el.tag === 'DEAT'; })[0].place;          
      
          res.render('modify', vars);
        }
      });

    }
  });
  */
});


app.get('/tree', (req, res) => {
  res.render('tree');
});


['edit-panel'].forEach(url => {
  app.get('/views/' + url, (req, res) => {
    res.sendFile( (__dirname + '/views/' + url + '.html') );
  });
});

// Init server

app.listen(app.get('port'), () => {
  console.log('Server started \n:) \n'.magenta + ('=> Listening on port: ' + app.get('port') ).green);
});

