PersonLog.remove().exec();
FamilyLog.remove().exec();
NoteLog.remove().exec();
SourceLog.remove().exec();

const getByTag = function(obj, str) {
  return _.reduce(_.filter(obj, (item) => { 
      return item.tag === str; })[0], (prev, curr) => {
        typeof curr === 'object' ? curr = '' : null;
        return prev + curr + '<br>';
    }, '');
};

swedish.people.forEach( (person) => {
  var entry = new PersonLog({ 
    id: person.id,
    phone: person.phon,
    familiesBornInto: person.famc,
    familiesStarted: person.fams,
    address: _.reduce(person.addr, (prev, curr) => { return prev + curr + '<br>'; }),
    nameFirst: person.names[0].value.slice(0, person.names[0].value.indexOf('/')),
    nameLast: person.names[0].value.slice(person.names[0].value.indexOf('/') + 1, person.names[0].value.length - 1),
    events: person.eventsFacts,
    noteRefs: person.noteRefs
  });
  entry.save(function(err) {
    if (err) {
      console.log(err);
    } else {
      console.log(">>>>>>>", entry);
    }
  });
});

swedish.families.forEach( (family) => {
  console.log("^^^^^^^^^^",family);
  const entry = new FamilyLog({
    id: family.id,
    husbandId: family.husbandRefs[0].ref,
    wifeId: (family.wifeRefs === undefined) ? 'undefined' : family.wifeRefs[0].ref,
    childIds: family.childRefs[0].ref,
    events: family.eventsFacts
  });
  entry.save(function(err) {
    if(err){
      console.log(err);
    } else {
      console.log("$$$$ $$$$ $$$$", entry);
    }
  });
});


swedish.notes.forEach( (note) => {


  const entry = new NoteLog({
    id: note.id,
    value: note.value,
    sourceCitationsUnderValue: note.sourceCitationsUnderValue
  });

  entry.save(function(err) {
    if(err){
      console.log(err);
    } else {
      console.log("$(((((((((((($$$ $$$$ $$$$))))))))))))", entry);
    }
  });

});


swedish.sources.forEach( (source) => {

  const entry = new SourceLog({
    id: source.id,
    value: source.value,
    repo: source.repo
  });

  entry.save(function(err) {
    if(err){
      console.log(err);
    } else {
      console.log("$$$$ $$$$ $$$$", entry);
    }
  });

});



function ObjToSource(o){
   if (!o) return 'null';
   if (typeof(o) == "object") {
       if (!ObjToSource.check) ObjToSource.check = new Array();
       for (var i=0, k=ObjToSource.check.length ; i<k ; ++i) {
           if (ObjToSource.check[i] == o) {return '{}';}
       }
       ObjToSource.check.push(o);
   }
   var k="",na=typeof(o.length)=="undefined"?1:0,str="";
   for(var p in o){
       if (na) k = "'"+p+ "':";
       if (typeof o[p] == "string") str += k + "'" + o[p]+"',";
       else if (typeof o[p] == "object") str += k + ObjToSource(o[p])+",";
       else str += k + o[p] + ",";
   }
   if (typeof(o) == "object") ObjToSource.check.pop();
   if (na) return "{"+str.slice(0,-1)+"}";
   else return "["+str.slice(0,-1)+"]";
}




