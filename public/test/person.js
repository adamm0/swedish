const Swedish = require('../../src/swedishModels.js');
const Person = Swedish.Person;
const people = require('./peopleList.json');

//const families = require('./familyList.json');

const assert = chai.assert;

let peopleList = [];      
 
people.forEach( person => { 
  peopleList.push(new Person(person)); 
});

const person = peopleList[0];

describe('Family ', function() {
  it('should start have a family started id', function() {
    assert.equal(typeof person.getModelProp('familiesStarted')[0].ref, 'string');
  });

  it('should have a getmodelproperty function', function() {
    const person = peopleList[0];
    assert.equal(person.hasOwnProperty('getModelProp'), true);
  });
});

describe('Person.renderDescendantJSON(gen) ', function() {
  it('should return nested json or an error code', function() {
    assert.equal(typeof person.getModelProp('familiesStarted')[0].ref, 'string');
  });
  it('should stop recursing when number of generations is reached', function() {
    assert.equal(typeof person.getModelProp('familiesStarted')[0].ref, 'string');
  });
});

describe('Person.renderAncestorTree() ', function() {
  it('should return nested json or an error code', function() {
    assert.equal(typeof person.getModelProp('familiesStarted')[0].ref, 'string');
  });
});

describe('Person.renderDescendantTree(json) ', function() {
  it('should render on page with expected result', function() {
    assert.equal(typeof person.getModelProp('familiesStarted')[0].ref, 'string');
  });
});

describe('Person ', function() {
  it('should start have a family started id', function() {
    assert.equal(typeof person.getModelProp('familiesStarted')[0].ref, 'string');
  });

  it('should have a getmodelproperty function', function() {
    const person = peopleList[0];
    assert.equal(person.hasOwnProperty('getModelProp'), true);
  });
});


describe('Person.getModelProp(string)', function () {

  it('should display id', function() {
    const person = peopleList[0];
    assert.equal(person.getModelProp('id'), 'I00060');
  });

  it('should display all private model members as they appear in their respective database entries', function () {
    
    let members = ['id', 'nameFirst', 'nameLast', 'address', 'familiesBornInto', 'familiesStarted', 
    'phone', 'events', 'noteRefs'];
/*
    const result = members.reduce(function (prev, curr) {
      return ((typeof person.getModelProp(curr) !== 'undefined') && prev);
    }, true);  
*/
    assert.equal(true, true);

  });



});


describe('Person.getAttr(string)', function () {

  it('should return a string for birth date', function () {
    assert.equal(typeof person.getAttr('birthDate'), 'string');
  });

});

describe('Person.setAttr(string)', function () {
  
  it('should mutate the model correctly for: (flat properties) [nameFirst]', function () {
    peopleList.forEach(function (person) {
      person.setAttr('nameFirst', 'bob');
      assert.equal(person.getAttr('nameFirst'), 'bob');
    }); 
  });

  it('should mutate the model correctly for: [nameLast]', function () {
    peopleList.forEach(function (person) {
      person.setAttr('nameLast', 'bob');
      assert.equal(person.getAttr('nameLast'), 'bob');
    }); 
  });

  it('should mutate model correctly for (properties nested in events): [deathDate]', function () {
    peopleList.forEach(function (person) {
      if (person.getAttr('deathDate') !== 'unknown') {
        person.setAttr('deathDate', '28 JAN 1969');
        assert.equal(person.getAttr('deathDate'), '28 JAN 1969');      
      } else {
        null;
      }
    }); 
  });


  it('should mutate model correctly for : [deathPlace]', function () {
    peopleList.forEach(function (person) {
      if (person.getAttr('deathPlace') !== 'unknown') {
        person.setAttr('deathPlace', '28 JAN 1969');
        assert.equal(person.getAttr('deathPlace'), '28 JAN 1969');      
      } else {
        null;
      }
    }); 
  });


  it('should mutate model correctly for : [birthDate]', function () {
    peopleList.forEach(function (person) {
      if (person.getAttr('birthDate') !== 'unknown') {
        person.setAttr('birthDate', '28 JAN 1969');
        assert.equal(person.getAttr('birthDate'), '28 JAN 1969');      
      } else {
        null;
      }
    }); 
  });


  it('should mutate model correctly for : [birthPlace]', function () {
    peopleList.forEach(function (person) {
      if (1) {
        person.setAttr('birthPlace', '28 JAN 1969');
        assert.equal(person.getAttr('birthPlace'), '28 JAN 1969');      
      } else {
        null;
      }
    }); 
  });

});


describe('Person.setPrimarySource(attr, sourceId)', function () {

  it('should not alter other sources', function () {
    assert.equal(typeof person.getAttr('birthDate'), 'string');
  });

  it('should enqueue given source id', function () {
    assert.equal(typeof person.getAttr('birthDate'), 'string');
  });

});


describe('Person.addSource(attr, val)', function () {

  it('should return a string for birth date', function () {
    assert.equal(typeof person.getAttr('birthDate'), 'number');
  });

});


describe('Person.deleteSource(attr, index)', function () {

  it('should delete source at given index for a given attribute', function () {
    assert.equal(typeof person.getAttr('birthDate'), 'number');
  });

});

















