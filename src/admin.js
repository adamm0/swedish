'use strict';
const _ = require('lodash');
const unique = require('uniq');
const $ = require('jquery');
const jQuery = require('jquery');
require('typeahead.js');
window.$ = window.jQuery = jQuery;
require('bootstrap');
require('bootstrap-material-design/dist/js/material.js');
require('../node_modules/bootstrap-material-design/dist/js/ripples.js');

//const Person = require('swedish-class-person.js');

//const url = 'http://127.0.0.1:5000'; 
const url = 'http://agile-fortress-79861.herokuapp.com';


function init(cb) {

  $.ajaxSetup({
    beforeSend: function() {
       $('.spinner').show();
    },
    complete: function(){
       $('.spinner').hide();
    },
    success: function() {
      console.log('success!!!');
    },
    contentType: 'application/json; charset=utf-8',
    timeout: 3000, 
    retryAfter:4000,
    error: ( function(){ func( param ) }, $.ajaxSetup().retryAfter )
  });


    //new
    $('#test-well').click(function(e) {
      $(this).fadeToggle();
      setTimeout(function(){ $('#test-input').fadeToggle(); }, 500);

    });

    $('#cancel').click(function() {
      $(this).closest('#test-input').fadeToggle();
        setTimeout(function(){ $('#test-well').fadeToggle(); }, 500);
      });
  

  }

$(document).ready(function()  {
  
  $.material.init();

  init();
  $('.spinner').hide();
  
});
