'use strict';
const _ = require('lodash');
const unique = require('uniq');
const $ = require('jquery');
const jQuery = require('jquery');
window.$ = window.jQuery = jQuery;
require('bootstrap');
require('proxy-polyfill');
require('bootstrap-material-design/dist/js/material.js');
const swedish = require('./swedish-framework.js');

const url = 'http://127.0.0.1:5000';
//const url = 'http://agile-fortress-79861.herokuapp.com';

$(document).ready(function() {

  $.material.init();

  $('#button').click(event => { 
    event.preventDefault();
    document.cookie = "username=" + document.getElementById("username").value;
    const len = document.getElementById("username").value.length;
    window.location = "http://" + document.getElementById("username").value + ":" + document.getElementById("password").value + "@127.0.0.1:5000/secure";
  });
});