'use strict';

const _ = require('lodash');

class Router {
  constructor () {
    this.config = {};
    this.routes = function (routes) {
      $(window).on('hashchange', function () { 
        const hash = window.location.hash;
        const type = hash[2];
        let id, route;

        if (type === 'I') {
          id = hash.slice(2, 8);
          route = '/' + hash.slice(8, hash.length);
        } else if (type === 'F') {
          id = hash.slice(2, 4);
          route = '/' + hash.slice(4, hash.length);
        }
        
        // ADD NOTES + SOURCES TYPE CONDITION

        console.log(routes, route, type, id);

        routes[route](route, type, id); 
      });
    };
  }
}

Router.prototype.renderView = function(route, varObj, success, customTarget) {
  const target = (typeof customTarget !== undefined) ? customTarget : app.config['target'];
  $.get(url + "/app" + route + ".html", function(data) {
    success();
    _.forEach(varObj, arr => { arr[0].initDataBind( (arr[1] )); });
  });
};

exports.Router = Router;