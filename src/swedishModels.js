'use strict';
const d3 = require('d3');

const _ = require('lodash');


class SwedishModel {
  constructor () {}
}
  
SwedishModel.prototype.getByTag = function(obj, str) {
  let result;
  const matching = _.filter(obj, (item) => { return item.tag === str; })[0];
  /*
  if (matching !== undefined) {
    result = _.reduce(matching, (prev, curr, i) => {
      (i === 'tag' || typeof curr === 'object') ? curr = '' : null;
      return prev + curr ;
    }, '');
  }     
  return result;
  */
  return matching;
};

SwedishModel.prototype.modifyEvents = function(tag, obj) {
  obj = _.extend(_.filter(this.events, event => { return event.tag === tag; } )[0], obj);
  this.setModelProp('events', _.extend(this.events, obj));
};

SwedishModel.prototype.initDataBind = function(elem) {
  //$.data( elem, "binding", this );

  _.forEach(this.getModelJSON(), (prop, key) => {
    $(elem).find('.' + key).attr('id', this.getModelProp('id') + key + prop);
    this.setModelProp(key, this.getModelProp(key));
  });
};

// ex:
// const husband = _.filter(families, family => {return husband.id === id; });
// husband.initDataBind( $('.husband') );

SwedishModel.prototype.save = function(className, cb) {
  console.log(JSON.stringify(this.getModelJSON()));
  $.post('http://127.0.0.1:5000' + '/api/' + className, JSON.stringify({ person: this.getModelJSON() }), 'json')
    .done(res => {
      console.log('success saved', res);
      cb();
    });
};

//Add admin save

exports.SwedishModel = SwedishModel;

class Family {
  constructor(family, peopleList) {

    /*    Private    */

    let _model = {
      id : family.id,
      husbandId : family.husbandId,
      wifeId : family.wifeId,
      events : family.events,
      eventSources : family.events.sourceCitations,
      noteRefs : family.noteRefs,
      childRefs : family.childIds
    };

    const _handler = {
      get: function(target, name) {
        return (target[name]);
      },
      set: function(target, prop, value) {
        let id = target[id];

        //if prop == events, 
       // $('.' + id + ).val(somethit else);

        //else
        $('#' + id + prop).val(value);

        target[prop] = value;
      }
    };

    let _modelProxy = new Proxy(_model, _handler);

    /*    Public    */

    this.getModelJSON = function() { return _model; };
    this.getModelProp = function(key) { return _model[key]; };
    this.setModelProp = function(key, value) { 
      _model[key] = value; 
    };

    this.id = family.id;
    this.husband = _.filter(peopleList, person => { return person.id === family.husbandId; })[0];
    this.wife = _.filter(peopleList, person => { return person.id === family.wifeId; })[0];
    this.events = family.events;
    this.eventSources = family.events.sourceCitations;
    this.noteRefs = family.noteRefs;
    this.children = family.childIds;
  }
}

Family.prototype = Object.create(SwedishModel.prototype);

Family.prototype.getAttr = function(name) {
  if (name === 'marriageDate') {
    const data = _.filter(this.events, event => { return event.tag === 'MARR'; })[0];
    if (typeof data === 'undefined') {
      return '';
    } else if (data.date === 'undefined') {
      return '';
    } else {
      return data.date;
    }

  } else if (name === 'marriagePlace') {
    const data = _.filter(this.events, event => { return event.tag === 'MARR'; })[0];
    if (typeof data === 'undefined') {
      return '';
    } else if (data.place === 'undefined') {
      return '';
    } else {
      return data.place;
    }
  }
};


exports.Family = Family; 


/* 
Make sure shared info is updated in db across schemas
*/

class Person {
  constructor(obj, handler) {
    let _model = {
      id: obj.id,
      nameFirst: obj.nameFirst,
      nameLast: obj.nameLast,
      address: obj.address,
      familiesBornInto: _.extend([], obj.familiesBornInto),
      familiesStarted: obj.familiesStarted,
      phone: obj.phone ? obj.phone : undefined,
      events: obj.events,
      // eventsSources: // change in db?
      noteRefs: obj.noteRefs
    };

/*
    const _handler = {
      get: function(target, name) {
        return (target[name]);
      },
      set: function(target, prop, value) {
        const id = target[id];

      
        $('#' + id + prop).val(value);

        target[prop] = value;
      }
    };

    const _modelProxy = new Proxy(_model, _handler);
*/
    /*    Public    */

    this.getModelJSON = function() { return _model; };
    
    this.getModelProp = function(key) { 
      return _model[key]; 
    };

    this.setModelProp = function(key, value) { 
      _model[key] = value; 
    };

    this.setModelAttr = function (attr) {
      if (attr === 'nameFirst') {
        this.setModelProp('nameFirst', attr);
      } else if (1) {
        console.log('hi');
      }
    };


    // Public members, for data that is manipulated or bound in D3 but not saved in DB.

    this.id = obj.id;
    this.nameFirst = obj.nameFirst;
    this.nameLast = obj.nameLast;
    this.birthDate = this.getByTag(obj.events, 'BIRT') ? this.getByTag(obj.events, 'BIRT').date : undefined;
    this.birthPlace = this.getByTag(obj.events, 'BIRT') ? this.getByTag(obj.events, 'BIRT').place : undefined;
    this.deathDate = this.getByTag(obj.events, 'DEAT') ? this.getByTag(obj.events, 'DEAT').date : undefined;
    this.deathPlace = this.getByTag(obj.events, 'DEAT') ? this.getByTag(obj.events, 'DEAT').place : undefined;
    this.familiesBornInto = obj.familyBornInto;
    this.familiesStarted = obj.familyStarted;
    this.address = obj.address;
    this.phone = obj.phone;
    this.events = obj.events;
    this.eventsSources = obj.events.sourceCitations;
    this.noteRefs = obj.noteRefs;
  }
}

Person.prototype = Object.create(SwedishModel.prototype);

Person.prototype.eventMod = function (tag, key, val) {

    //shallow-copy events
    let newEvents = this.getModelProp('events');
    
    console.log('newEvents: ', newEvents);

    //find the index (or lack thereof) of tag in events
    let index = newEvents.map(event => { 
      return event.tag === tag;
    }).indexOf(true);

    console.log('index: ', index);

    if (index > 0) {
      console.log(newEvents, index, key);
      newEvents[index][key] = val;
      this.setModelProp('events', newEvents);
    } else {

      let obj = {
        tag: tag,
      };

      obj[key] = val;

      newEvents.unshift(obj);

      this.setModelProp('events', newEvents);

      console.log('err: could not find property. ccreatin it!');
    }

    console.log(index);

  };


Person.prototype.setAttr = function(name, val) {

  //check for primary properties 

  if (name === 'id') {

    this.setModelProp('id', val);

  } else if (name === 'nameFirst') {

    this.setModelProp('nameFirst', val);

  } else if (name === 'nameLast') {

    this.setModelProp('nameLast', val);

  } else if (name === 'address') {

    this.setModelProp('address', val);

  //check for nested properties

  } else if (name === 'birthDate') {

   this.eventMod('BIRT', 'date', val);

  } else if (name === 'familyBornInto') {

    //this does not delete old family but replaces it as primary

    let families = this.getModelProp('familiesBornInto');

    families.unshift(val);

    this.setModelProp('familiesBornInto', families);

  } else if (name === 'familyStarted') {

    //this does not delete old family but replaces it as primary
    let families = this.getModelProp('familiesStarted');

    families.unshift(val);

    this.setModelProp('familiesStarted', families);

  } else if (name === 'birthDate') {
    this.eventMod('BIRT', 'date', val);
  } else if (name === 'birthPlace') {
    this.eventMod('BIRT', 'place', val);
  } else if (name === 'deathDate') {
    this.eventMod('DEAT', 'date', val);
  } else if(name === 'deathPlace') {
    this.eventMod('DEAT', 'place', val);
  }

};

Person.prototype.getAttr = function (name) {

  let result;

  if (name === 'birthDate') {
    const data = _.filter(this.events, event => { return event.tag === 'BIRT' && event.hasOwnProperty('date'); })[0];
    if (typeof data === 'undefined') {
      result = '';
    } else if (data.date === 'undefined') {
      result = '';
    } else {
      result = data.date;
    }
  } else if (name === 'nameFirst') {
    result = this.getModelProp('nameFirst');
  } else if (name === 'nameLast') {
    result = this.getModelProp('nameLast');
  } else if (name === 'birthPlace') {
    const data = _.filter(this.getModelProp('events'), event => { return event.tag === 'BIRT'; })[0];
    if (typeof data === 'undefined') {
      result = '';
    } else if (data.place === 'undefined') {
      result = '';
    } else {
      result = data.place;
    }
  } else if (name === 'deathPlace') {
    const data = _.filter(this.getModelProp('events'), event => { return event.tag === 'DEAT'; })[0];
    if (typeof data === 'undefined') {
      result = '';
    } else if (data.place === 'undefined') {
      result = '';
    } else {
      result = data.place;
    }
  } else if (name === 'deathDate') {
    const data = _.filter(this.getModelProp('events'), event => { return event.tag === 'DEAT'; })[0];
    if (typeof data === 'undefined') {
      result = '';
    } else if (data.date === 'undefined') {
      result = '';
    } else {
      result = data.date;
    }
  } else if (name === 'sex') {
    const data = _.filter(this.getModelProp('events'), event => { return event.tag === 'SEX'; })[0];
    if (typeof data === 'undefined') {
      result = '';
    } else if (data.value === 'undefined') {
      result = '';
    } else {
      result = data.value;
    }
  }

  return result;

};


Person.prototype.renderAncestorTree = function (generations, familyList, peopleList) {

  let gen = generations;

  const ancestorTree = {
    _id: this.getModelProp('id'),
    name: this.getModelProp('nameFirst') + this.getModelProp('nameLast'),
    children: []
  };

  bob(ancestorTree);

  function bob (tree) {

    // get target person

    let person = _.filter(peopleList, person => { 
      return person.getModelProp('id') === tree._id; 
    });

    if (person.length < 1) {
      console.log('couldnt find person');
      return 0;
    } else {
      person = person[0];
    }

    // get family they were born into

    let family = person.getModelProp('familiesBornInto');

    if (family.length < 1) {
      console.log('person didnt have parents listed :(');
      return 0;
    } else {
      family = _.filter(familyList, fam => { 
        return fam.getModelProp('id') === family[0].ref;
      })[0];
    }

    if (typeof family === 'undefined') {
      console.log('couldnt find fam from id');
      return;
    }

    const husband = peopleList.filter(person => {
      return person.getModelProp('id') === family.getModelProp('husbandId');
    });

    const wife = peopleList.filter(person => {
      return person.getModelProp('id') === family.getModelProp('wifeId');
    });

    [husband, wife].forEach(parent => {
      if (parent.length > 0) {
        tree.children.push({
          name: parent[0].getModelProp('nameFirst') + parent[0].getModelProp('nameLast'),
          _id: parent[0].getModelProp('id'),
          children: []
        });
      }
    });

    if (gen > 0) {
      //gen -= 1;

      tree.children.forEach(child => { 
        bob(child); 
      });
    }

    else {
      return ;
    }

  }

  console.log(ancestorTree, "jj");

  let tree = {};

  function move (tree) {
    tree.children[3];
  }


  return ancestorTree;

};

Person.prototype.renderDescendantTree = function (generations, familyList, peopleList) {

  let gen = generations;

  const descendantTree = {
    _id: this.getModelProp('id'),
    name: this.getModelProp('nameFirst') + this.getModelProp('nameLast')
  };

  bob(descendantTree);

  function bob (tree) {

    console.log(gen);

    let person = _.filter(peopleList, person => { 
      return person.getModelProp('id') === tree._id; 
    });

    if (person.length < 1) {
      console.log('couldnt find person');
      return 0;
    } else {
      person = person[0];
    }

    let family = person.getModelProp('familiesStarted');

    if (family.length < 1) {
      console.log('couldnt find family');
      return 0;
    } else {
      family = _.filter(familyList, fam => { 
        return fam.getModelProp('id') === family[0].ref;
      })[0];
    }

    if (typeof family === 'undefined') {
      console.log('couldnt find fam from id');
      return;
    }

    tree.children = family.getModelProp('childRefs')
                    .map(child => {
                      return peopleList.filter(person => {
                        return person.getModelProp('id') === child.ref;
                      })[0];
                    })
                    .map(child => { 
                      return {
                        _id: child.getModelProp('id'),
                        name: child.getModelProp('nameFirst') + child.getModelProp('nameLast'),
                        children: []
                      }; 
                    });
                    console.log(tree);

    if (gen > 0) {
      //gen -= 1;

      tree.children.forEach(child => { 
        bob(child); 
      });
    }

    else {
      return ;
    }

  }
  console.log(descendantTree, "jj");

  return descendantTree;

};

Person.prototype.descendantTree = function (gen, familyList, peopleList) {

  var tooltip = d3.select("body")
    .append("div")
    .style("position", "absolute")
    .style("z-index", "10")
    .style("visibility", "hidden")
    .style("opacity", ".8")
    .style("padding", "5px")
    .style("border-radius", "2px")
    .style("width", "250px")
    .style("height", "auto")
    .style("background-color", "#dedede")
    .text("a simple tooltip");

var margin = {top: 20, right: 120, bottom: 20, left: 120},
    width = 10000 - margin.right - margin.left,
    height = 10000 - margin.top - margin.bottom;

var i = 0,
    duration = 750,
    root;

var tree = d3.layout.tree()
    .nodeSize([135, 35])
    .separation(function(a, b) { return (a.parent == b.parent ? 1 : 2); });

function zoomed() {
  container.attr("transform", "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")");
}

var zoom = d3.behavior.zoom()
    .scaleExtent([1, 10])
    .on("zoom", zoomed);

var diagonal = d3.svg.diagonal();

var svg = d3.select("body").append("svg")
    .attr("width", width + margin.right + margin.left)
    .attr("height", height + margin.top + margin.bottom)
    .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
    svg.call(zoom);

let jsonTree = this.renderDescendantTree(gen, familyList, peopleList);

  root = jsonTree;
  root.x0 = height / 2;
  root.y0 = 0;

  function collapse(d) {
    if (d.children) { /*
      d._children = d.children;
      d._children.forEach(collapse);
      d.children = null;*/
    }
  }

  //root.children.forEach(collapse);
  update(root);


d3.select(self.frameElement).style("height", "800px");

function update(source) {

  // Compute the new tree layout.
  var nodes = tree.nodes(root).reverse(),
      links = tree.links(nodes);

  // Normalize for fixed-depth.
  nodes.forEach(function(d) { d.y = d.depth * 180; });

  // Update the nodes…
  var node = svg.selectAll("g.node")
      .data(nodes, function(d) { return d.id || (d.id = ++i); });

  // Enter any new nodes at the parent's previous position.
  var nodeEnter = node.enter().append("g")
      .attr("class", "node")
      .attr("transform", function(d) { return "translate(" + source.x0 + "," + source.y0 + ")"; })
      .on("click", click)
      .on("mouseover", function(ev) { 
        const person = _.filter(peopleList, person => { return person.getModelProp('id') === ev._id; })[0];
        console.log('is this it?!', ev);
        return tooltip.style("visibility", "visible").html("<h4 class='text-center'>" + person.getAttr('nameFirst') + person.getAttr('nameLast') + '</h4>' +  "<p>" + 'Birth Date: ' + person.getAttr('birthDate') + "</p>" + "<p>" + 'Birth Place: ' + person.getAttr('birthPlace') + "</p>" + 'Death Date: ' + person.getAttr('deathDate') + "</p>" + 'Death Place: ' + person.getAttr('deathPlace') + "</p>" );
      })
      .on("mousemove", function() { 
        return tooltip.style("top",(d3.event.pageY-10)+"px").style("left",(d3.event.pageX+10)+"px");
      })
      .on("mouseout", function() {
        return tooltip.style("visibility", "hidden");
      });





      var zoom = d3.behavior.zoom()
      .on("zoom", update);

  nodeEnter.append("rect")
      .attr('x', -65)
      .attr('y', 0)
      .attr('width', 130)
      .attr('height', 30)
      .style("fill", function(d) { return d._children ? "lightsteelblue" : "#fff"; })
      .call(zoom);



  nodeEnter.append("text")
      .attr("x", '0')
      .attr("y", '10')
      .attr("dy", ".35em")
      .attr("text-anchor", 'middle')
      .text(function(d) { return d.name; })
      .style("fill-opacity", 1e-6);

    nodeEnter.append("text")
      .attr("x", '0')
      .attr("y", '-10')
      .attr("dy", ".35em")
      .attr("text-anchor", 'middle')
      .text(function(d) { 

        let person = _.filter(peopleList, person => {
          return person.getAttr('id') === d._id;
        })[0];

        person = person ? person : { getAttr: () => { return ''; } };
        console.log("@@@", d, person);

        return ( '' + person.getAttr('birthDate') + ' - ' + person.getAttr('deathDate') ); 
      })
      .style("fill-opacity", 1e-6);



  // Transition nodes to their new position.
  var nodeUpdate = node.transition()
      .duration(duration)
      .attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; });

  nodeUpdate.select("rect")
      .attr("r", 4.5)
      .style("fill", function(d) { return d._children ? "lightsteelblue" : "#fff"; });

  nodeUpdate.select("text")
      .style("fill-opacity", 1);

  // Transition exiting nodes to the parent's new position.
  var nodeExit = node.exit().transition()
      .duration(duration)
      .attr("transform", function(d) { return "translate(" + source.x + "," + source.y + ")"; })
      .remove();

  nodeExit.select("rect")
      .attr("r", 1e-6);

  nodeExit.select("text")
      .style("fill-opacity", 1e-6);

  // Update the links…
  var link = svg.selectAll("path.link")
      .data(links, function(d) { return d.target.id; });

  // Enter any new links at the parent's previous position.
  link.enter().insert("path", "g")
      .attr("class", "link")
      .attr("d", function(d) {
        var o = {x: source.x0, y: source.y0};
        return diagonal({source: o, target: o});
      });

  // Transition links to their new position.
  link.transition()
      .duration(duration)
      .attr("d", diagonal);

  // Transition exiting nodes to the parent's new position.
  link.exit().transition()
      .duration(duration)
      .attr("d", function(d) {
        var o = {x: source.x, y: source.y};
        return diagonal({source: o, target: o});
      })
      .remove();

  // Stash the old positions for transition.
  nodes.forEach(function(d) {
    d.x0 = d.x;
    d.y0 = d.y;
  });
}

// Toggle children on click.
function click(d) {
  if (d.children) {
    d._children = d.children;
    d.children = null;
  } else {
    d.children = d._children;
    d._children = null;
  }
  update(d);
}
};


Person.prototype.ancestorTree = function (gen, familyList, peopleList) {


  var tooltip = d3.select("body")
    .append("div")
    .style("position", "absolute")
    .style("z-index", "10")
    .style("visibility", "hidden")
    .style("opacity", ".8")
    .style("padding", "5px")
    .style("border-radius", "2px")
    .style("width", "250px")
    .style("height", "auto")
    .style("background-color", "#dedede")
    .text("a simple tooltip");

var margin = {top: 20, right: 120, bottom: 20, left: 120},
    width = 2200 - margin.right - margin.left,
    height = 2200 - margin.top - margin.bottom;

var i = 0,
    duration = 750,
    root;

var tree = d3.layout.tree()
    .size([height, width]);

var diagonal = d3.svg.diagonal()
    .projection(function(d) { return [d.y, d.x]; });

var svg = d3.select("body").append("svg")
    .attr("width", width + margin.right + margin.left)
    .attr("height", height + margin.top + margin.bottom)
  .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
let jsonTree = this.renderAncestorTree(gen, familyList, peopleList);


  root = jsonTree;
  root.x0 = height / 2;
  root.y0 = 0;

  function collapse(d) {/*
    if (d.children) {
      d._children = d.children;
      d._children.forEach(collapse);
      d.children = null;
    }*/
  }

  //root.children.forEach(collapse);
  update(root);


d3.select(self.frameElement).style("height", "800px");

function update(source) {

  // Compute the new tree layout.
  var nodes = tree.nodes(root).reverse(),
      links = tree.links(nodes);

  // Normalize for fixed-depth.
  nodes.forEach(function(d) { d.y = d.depth * 180; });

  // Update the nodes…
  var node = svg.selectAll("g.node")
      .data(nodes, function(d) { return d.id || (d.id = ++i); });

  // Enter any new nodes at the parent's previous position.
  var nodeEnter = node.enter().append("g")
      .attr("class", "node")
      .attr("transform", function(d) { return "translate(" + source.y0 + "," + source.x0 + ")"; })
      .on("click", click)
      .on("mouseover", function(ev){ 
        const person = _.filter(peopleList, person => { return person.getModelProp('id') === ev._id; })[0];
        console.log('is this it?!', ev);
        return tooltip.style("visibility", "visible").html("<h4 class='text-center'>" + person.getAttr('nameFirst') + person.getAttr('nameLast') + '</h4>' +  "<p>" + 'Birth Date: ' + person.getAttr('birthDate') + "</p>" + "<p>" + 'Birth Place: ' + person.getAttr('birthPlace') + "</p>" + 'Death Date: ' + person.getAttr('deathDate') + "</p>" + 'Death Place: ' + person.getAttr('deathPlace') + "</p>" );
      })
      .on("mousemove", function() { 
        return tooltip.style("top",(d3.event.pageY-10)+"px").style("left",(d3.event.pageX+10)+"px");
      })
      .on("mouseout", function(){
        return tooltip.style("visibility", "hidden");
      });

  nodeEnter.append("rect")
      .attr("r", 1e-6)
      .style("fill", function(d) { return d._children ? "lightsteelblue" : "#fff"; });

  nodeEnter.append("text")
      .attr("x", function(d) { return d.children || d._children ? -10 : 10; })
      .attr("dy", ".35em")
      .attr("text-anchor", function(d) { return d.children || d._children ? "end" : "start"; })
      .text(function(d) { return d.name; })
      .style("fill-opacity", 1e-6);

  // Transition nodes to their new position.
  var nodeUpdate = node.transition()
      .duration(duration)
      .attr("transform", function(d) { return "translate(" + d.y + "," + d.x + ")"; });

  nodeUpdate.select("rect")
      .attr("r", 4.5)
      .style("fill", function(d) { return d._children ? "lightsteelblue" : "#fff"; });

  nodeUpdate.select("text")
      .style("fill-opacity", 1);

  // Transition exiting nodes to the parent's new position.
  var nodeExit = node.exit().transition()
      .duration(duration)
      .attr("transform", function(d) { return "translate(" + source.y + "," + source.x + ")"; })
      .remove();

  nodeExit.select("rect")
      .attr("r", 1e-6);

  nodeExit.select("text")
      .style("fill-opacity", 1e-6);

  // Update the links…
  var link = svg.selectAll("path.link")
      .data(links, function(d) { return d.target.id; });

  // Enter any new links at the parent's previous position.
  link.enter().insert("path", "g")
      .attr("class", "link")
      .attr("d", function(d) {
        var o = {x: source.x0, y: source.y0};
        return diagonal({source: o, target: o});
      });

  // Transition links to their new position.
  link.transition()
      .duration(duration)
      .attr("d", diagonal);

  // Transition exiting nodes to the parent's new position.
  link.exit().transition()
      .duration(duration)
      .attr("d", function(d) {
        var o = {x: source.x, y: source.y};
        return diagonal({source: o, target: o});
      })
      .remove();

  // Stash the old positions for transition.
  nodes.forEach(function(d) {
    d.x0 = d.x;
    d.y0 = d.y;
  });
}

// Toggle children on click.
function click(d) {
  if (d.children) {
    d._children = d.children;
    d.children = null;
  } else {
    d.children = d._children;
    d._children = null;
  }
  update(d);
}
};


exports.Person = Person; 



class Note {
  constructor(obj) {

    /*    Private    */

    const _model = {
      id : family.id,
      husband : _.filter(peopleList, person => { return person.id === family.husbandId; })[0],
      wife : _.filter(peopleList, person => { return person.id === family.wifeId; })[0],
      events : family.events,
      eventSources : family.events.sourceCitations,
      noteRefs : family.noteRefs,
      children : family.childIds
    };

    const _handler = {
      get: function(target, name) {
        return (target[name]);
      },
      set: function(target, prop, value) {
        const id = target[id];

        $('#' + id + prop).val(value);

        target[prop] = value;
      }
    };

    const _modelProxy = new Proxy(_model, _handler);

    /*    Public    */

    this.getModelJSON = function() { return _model; };
    this.getModelProp = function(key) { return _model[key]; };
    this.setModelProp = function(key, value) { 
      _model[key] = value; 
    };

    this.id = obj.id;
    this.value = obj.value;
    this.sourceCitationsUnderValue = obj.sourceCitationsUnderValue;
  }
}

Note.prototype = Object.create(SwedishModel.prototype);

exports.Note = Note;

class Source {
  constructor(obj) {

    /*    Private    */

    const _model = {
      id: obj.id,
      title: obj.title,
      repo: obj.repo
    };

    const _handler = {
      get: function(target, name) {
        return (target[name]);
      },
      set: function(target, prop, value) {
        const id = target[id];

        $('#' + id + prop).val(value);

        target[prop] = value;
      }
    };

    const _modelProxy = new Proxy(_model, _handler);

    /*    Public    */

    this.getModelJSON = function() { return _model; };
    this.getModelProp = function(key) { return _model[key]; };
    this.setModelProp = function(key, value) { 
      _model[key] = value; 
    };
    this.id = obj.id;
    this.title = obj.title;
    this.repo = obj.repo;
  }
}

Source.prototype = Object.create(SwedishModel.prototype);

exports.Source = Source; 





