'use strict';

const _ = require('lodash');

class Router {
  constructor () {
    this.config = {};
    this.routes = function (routes) {
      $(window).on('hashchange', function () { 
        const hash = window.location.hash;
        const type = hash[2];
        let id, route;

        if (type === 'I') {
          id = hash.slice(2, 8);
          route = '/' + hash.slice(8, hash.length);
        } else if (type === 'F') {
          id = hash.slice(2, 4);
          route = '/' + hash.slice(4, hash.length);
        }
        
        // ADD NOTES + SOURCES TYPE CONDITION

        console.log(routes, route, type, id);

        routes[route](route, type, id); 
      });
    };
  }
}

Router.prototype.renderView = function(route, varObj, success, customTarget) {
  debugger;
  const target = (typeof customTarget !== undefined) ? customTarget : app.config['target'];
  $.get(url + "/app" + route + ".html", function(data) {
    success();
    _.forEach(varObj, arr => { arr[0].initDataBind( arr[1] ); });
  });
};

exports.Router = Router;

class SwedishModel {
  constructor () {}
}

SwedishModel.prototype.getByTag = function(obj, str) {
  let result;
  const matching = _.filter(obj, (item) => { return item.tag === str; })[0];
  /*
  if (matching !== undefined) {
    result = _.reduce(matching, (prev, curr, i) => {
      (i === 'tag' || typeof curr === 'object') ? curr = '' : null;
      return prev + curr ;
    }, '');
  }     
  return result;
  */
  return matching;
};

SwedishModel.prototype.modifyEvents = function(tag, obj) {
  obj = _.extend(_.filter(this.events, event => { return event.tag === tag; } )[0], obj);
  this.setModelProp('events', _.extend(this.events, obj));
};

SwedishModel.prototype.initDataBind = function(elem) {
  // $.data( $(elem), "binding", this );
    debugger;

  _.forEach(this.getModelJSON(), (prop, k) => {
    let y = $( app.config['target'] + elem).closest('.' + k);
    $(elem).closest('.' + k).attr('id', this.getModelProp('id') + k );
    console.log(y,$(elem).closest('.' + k), $(elem) );
    this.setModelProp(k, this.getModelProp(k));
  });
};

// ex:
// const husband = _.filter(families, family => {return husband.id === id; });
// husband.initDataBind( $('.husband') );

SwedishModel.prototype.save = function(className) {
  $.post(url + '/api/' + className, this.getModelJSON(), 'json')
    .done(res => {
      console.log('success', res);
    });
};

//Add admin save

exports.SwedishModel = SwedishModel;

class Family {
  constructor(family, peopleList) {

    /*    Private    */

    const _model = {
      id : family.id,
      husband : _.filter(peopleList, person => { return person.id === family.husbandId; })[0],
      wife : _.filter(peopleList, person => { return person.id === family.wifeId; })[0],
      events : family.events,
      eventSources : family.events.sourceCitations,
      noteRefs : family.noteRefs,
      children : family.childIds
    };

    const _handler = {
      get: function(target, name) {
        return (target[name]);
      },
      set: function(target, prop, value) {
        const id = target[id];

        //if prop == events, 
       // $('.' + id + ).val(somethit else);

        //else
        $('#' + id + prop).val(value);

        target[prop] = value;
      }
    };

    const _modelProxy = new Proxy(_model, _handler);

    /*    Public    */

    this.getModelJSON = function() { return _modelProxy; };
    this.getModelProp = function(key) { return _modelProxy[key]; };
    this.setModelProp = function(key, value) { 
      _modelProxy[key] = value; 
    };

    this.id = family.id;
    this.husband = _.filter(peopleList, person => { return person.id === family.husbandId; })[0];
    this.wife = _.filter(peopleList, person => { return person.id === family.wifeId; })[0];
    this.events = family.events;
    this.eventSources = family.events.sourceCitations;
    this.noteRefs = family.noteRefs;
    this.children = family.childIds;
  }
}

Family.prototype = Object.create(SwedishModel.prototype);

exports.Family = Family; 


/* 
Make sure shared info is updated in db across schemas
*/


class Person {
  constructor(obj, handler) {

    const _model = {
      id: obj.id,
      nameFirst: obj.nameFirst,
      nameLast: obj.nameLast,
      address: obj.address,
      familyBornIntoID: obj.familiesBornInto ? obj.familiesBornInto[0].ref : undefined,
      familyStartedID: obj.familiesStarted ? obj.familiesStarted[0].ref : undefined,
      phone: obj.phone ? obj.phone : undefined,
      events: obj.events,
      // eventsSources: // change in db?
      noteRefs: obj.noteRefs
    };

    const _handler = {
      get: function(target, name) {
        return (target[name]);
      },
      set: function(target, prop, value) {
        const id = target[id];

        //if prop == events, 
       // $('.' + id + ).val(somethit else);

        //else
        $('#' + id + prop).val(value);

        target[prop] = value;
      }
    };

    const _modelProxy = new Proxy(_model, _handler);

    /*    Public    */

    this.getModelJSON = function() { return _modelProxy; };
    this.getModelProp = function(key) { return _modelProxy[key]; };
    this.setModelProp = function(key, value) { 
      _modelProxy[key] = value; 
    };


    // Public members, for data that is manipulated or bound in D3 but not saved in DB.

    this.id = obj.id;
    this.nameFirst = obj.nameFirst;
    this.nameLast = obj.nameLast;
    this.birthDate = this.getByTag(obj.events, 'BIRT') ? this.getByTag(obj.events, 'BIRT').date : undefined;
    this.birthPlace = this.getByTag(obj.events, 'BIRT') ? this.getByTag(obj.events, 'BIRT').place : undefined;
    this.deathDate = this.getByTag(obj.events, 'DEAT') ? this.getByTag(obj.events, 'DEAT').date : undefined;
    this.deathPlace = this.getByTag(obj.events, 'DEAT') ? this.getByTag(obj.events, 'DEAT').place : undefined;
    this.familyBornInto = obj.familyBornInto ? obj.familyBornInto[0].ref : undefined;
    this.familyStarted = obj.familyStarted ? obj.familyStarted[0].ref : undefined;
    this.address = obj.address;
    this.phone = obj.phone;
    this.events = obj.events;
    this.eventsSources = obj.events.sourceCitations;
    this.noteRefs = obj.noteRefs;
  }
}

Person.prototype = Object.create(SwedishModel.prototype);

exports.Person = Person; 



class Note {
  constructor(obj) {

    /*    Private    */

    const _model = {
      id : family.id,
      husband : _.filter(peopleList, person => { return person.id === family.husbandId; })[0],
      wife : _.filter(peopleList, person => { return person.id === family.wifeId; })[0],
      events : family.events,
      eventSources : family.events.sourceCitations,
      noteRefs : family.noteRefs,
      children : family.childIds
    };

    const _handler = {
      get: function(target, name) {
        return (target[name]);
      },
      set: function(target, prop, value) {
        const id = target[id];

        $('#' + id + prop).val(value);

        target[prop] = value;
      }
    };

    const _modelProxy = new Proxy(_model, _handler);

    /*    Public    */

    this.getModelJSON = function() { return _modelProxy; };
    this.getModelProp = function(key) { return _modelProxy[key]; };
    this.setModelProp = function(key, value) { 
      _modelProxy[key] = value; 
    };

    this.id = obj.id;
    this.value = obj.value;
    this.sourceCitationsUnderValue = obj.sourceCitationsUnderValue;
  }
}

Note.prototype = Object.create(SwedishModel.prototype);

exports.Note = Note;


class Source {
  constructor(obj) {

    /*    Private    */

    const _model = {
      id: obj.id,
      title: obj.title,
      repo: obj.repo
    };

    const _handler = {
      get: function(target, name) {
        return (target[name]);
      },
      set: function(target, prop, value) {
        const id = target[id];

        $('#' + id + prop).val(value);

        target[prop] = value;
      }
    };

    const _modelProxy = new Proxy(_model, _handler);

    /*    Public    */

    this.getModelJSON = function() { return _modelProxy; };
    this.getModelProp = function(key) { return _modelProxy[key]; };
    this.setModelProp = function(key, value) { 
      _modelProxy[key] = value; 
    };

    this.id = obj.id;
    this.title = obj.title;
    this.repo = obj.repo;
  }
}

Source.prototype = Object.create(SwedishModel.prototype);

exports.Source = Source; 





