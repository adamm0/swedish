function() {


  $('#hubby-name').text(this.husband.nameFirst + this.husband.nameLast);
  $('input#husband-birth-date.form-control').val(this.husband.birthDate);
  $('input#husband-birth-place.form-control').val(this.husband.birthPlace);
  $('input#husband-death-date').val(this.husband.deathDate);
  $('input#husband-death-place').val(this.husband.deathPlace);

  $('input#exampleInputName2.form-control').val(_.filter(this.events, event => { return event.tag === 'MARR'; })[0].date);
  $('input#exampleInputEmail2.form-control').val(_.filter(this.events, event => { return event.tag === 'MARR'; })[0].place);

  $('#wifey-name').text(this.wife.nameFirst + this.wife.nameLast);
  $('input#wife-birth-date.form-control').val(this.wife.birthDate);
  $('input#wife-birth-place.form-control').val(this.wife.birthPlace);
  $('input#wife-death-date').val(this.wife.deathDate);
  $('input#wife-death-place').val(this.wife.deathPlace);
/*
  const hubFam = _.filter(families, fam => { return fam.id === this.husband.familyBornInto.ref })[0];
  const wifeFam = this.wife.familyBornInto;

  $('div#husband-mother.well.admin-well').text(hubFam.wife.nameFirst + hubFam.wife.nameLast + " ");
  $('#husband-father').text(hubFam.husband.nameFirst + hubFam.husband.nameLast + " ");

  $('#wife-mother').text(hubFam.wife.nameFirst + hubFam.wife.nameLast + " ");
  $('#wife-father').text(hubFam.husband.nameFirst + hubFam.husband.nameLast + " ");
*/
  const matches = _.filter(peopleList, person => { 
    if (typeof person.familyBornInto !== 'undefined') {
      if(typeof person.familyBornInto.ref !== 'undefined') {
        return person.familyBornInto.ref === this.id; 
      }
    } else {
      return false;
    }
  });

  debugger;

  console.log("MATCHES: ", matches);


$('table tr').remove();
$('table td').remove();
$('table th').remove();
$('table').prepend("<th>Name</th><th>Sex</th><th>Birth Date</th><th>Birth Place</th><th>Death Date</th><th>Death Place</th>");

  _.forEach(matches, (match) => { 
    const name = match.nameFirst + match.nameLast;
    const sex = _.filter(match.events, event => { return event.tag === 'SEX'; })[0].value;
    const birthdate = match.birth.slice(0,11);
    const birthplace = match.birth.slice(11,match.length);
    const deathdate = match.death.slice(0,11);
    const deathplace = match.death.slice(11,match.length);
    const id = match.id;
    $('table').append("<tr>" + 
      "<td>" + "<a href='/modify#" + id + "" + "'>" + name + "</a>" + "</td>" + 
      "<td>" + sex + "</td>" + 
      "<td>" + birthdate + "</td>" + 
      "<td>" + birthplace + "</td>" + 
      "<td>" + deathdate + "</td>" + 
      "<td>" + deathplace + "</td>" + "</tr>"); 
  });

