'use strict';
const _ = require('lodash');
const unique = require('uniq');
const $ = require('jquery');
const jQuery = require('jquery');
window.$ = window.jQuery = jQuery;
require('bootstrap');
require('proxy-polyfill');
require('bootstrap-material-design/dist/js/material.js');
require('../node_modules/bootstrap-material-design/dist/js/ripples.js');

const swedish = require('./swedish-framework.js');

const Person = swedish.Person;
const Family = swedish.Family;
const Note = swedish.Note;
const Source = swedish.Source;

//const url = 'http://127.0.0.1:5000';
const url = 'http://agile-fortress-79861.herokuapp.com';

$(document).ready(function() {

  const cookie = document.cookie;
  const username = cookie.slice(cookie.indexOf('=') + 1, cookie.length);

  $('#username').text(username);

  $.material.init();


  $.getJSON('/api/getdatabackups', function(json, textStatus) {
    
    $('#spinner').hide();
    
    _.forEach(json, function (item, i) {
      if (i < json.length - 1) {
        $('#backups').append('<a href="#" class="timestamp"><button class="btn btn-raised btn-lg withripple btn-primary" type="button">' + item + 
        '</button></a>');
      }
    });

    $('#backups').on('click', '.timestamp', function (event) {
      const timestamp = $(this).text();
      $('#spinner').show();
      $.ajax({
        url: '/api/restoredatabackups',
        type: 'POST',
        dataType: 'json',
        data: { timestamp: timestamp },
      })
      .done(function() {
        console.log("success");
        $('#spinner').hide();
      })
      .fail(function() {
        console.log("error");
      })
      .always(function() {
        console.log("complete");
      });
      
    });

  });

  $('.newbackup').click(function(event) {

    $('#backups').html('');

    $('#spinner').show();

    $.ajax({
      url: '/api/createdatabackup',
      type: 'POST',
      dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
      data: {param1: 'value1'},
    })
    .done(function() {
      console.log("success");
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {

      $.getJSON('/api/getdatabackups', function(json, textStatus) {
        $('#spinner').hide();
        _.forEach(json, function (item, i) {
          if (i < json.length - 1) {
            $('#backups').append('<a href="#" class="timestamp"><button class="btn btn-raised btn-lg withripple btn-primary" type="button">' + item + 
            '</button></a>');
          }
        });
      });

      

      console.log("complete");

    });
    
  });


});




