'use strict';
const Handlebars = require('../node_modules/handlebars/dist/cjs/handlebars.js');
const _ = require('lodash');
const d3 = require('d3');
const $ = require('jquery');
const jQuery = require('jquery');
window.$ = window.jQuery = jQuery;
require('bootstrap');
require('bootstrap-material-design/dist/js/material.js');
require('../node_modules/bootstrap-material-design/dist/js/ripples.js');

require('typeahead.js');
require('proxy-polyfill');
const swedishApp = require('./swedishRouter.js');
const swedishModels = require('./swedishModels.js');
 
global.app = new swedishApp.Router();

const Person = swedishModels.Person;
const Family = swedishModels.Family;
const Note = swedishModels.Note;
const Source = swedishModels.Source;

global.url = 'http://127.0.0.1:5000';

// 'http://agile-fortress-79861.herokuapp.com'
//const url = 'http://agile-fortress-79861.herokuapp.com';

let peopleList = [];
let familyList = [];
let sourceList = [];

function renderShit(templateStr, context1) {
  const editTemplateScript = $(templateStr).html();
  const editTemplate = Handlebars.compile(editTemplateScript);
  const context = context1;
  const editCompiledHtml = editTemplate(context);
  $('#target').html(editCompiledHtml);
}

function renderMe(templateStr, context, cb) {

  $.ajax({
    url: '/views/' + templateStr,
    type: 'GET',
    dataType: 'html',
  })
  .done(function(data) {

    const editTemplateScript = $(data).html();
    const editTemplate = Handlebars.compile(editTemplateScript);
    const editCompiledHtml = editTemplate(context);

    /* if (typeof target === 'undefined') {
      $('#target').html(editCompiledHtml);
    } else { */
      $('#target').toggle();
      $('#target').html(editCompiledHtml);
      $('#target').fadeToggle();
      $('#target').html(editCompiledHtml);
      if (typeof cb !== 'undefined') {
        cb();
      }
    console.log("success");
  })
  .fail(function() {
    console.log("error");
  })
  .always(function() {
    console.log("complete");
  });

}

app.config['target'] = $('.starter-template');

app.routes({

  '/edit': function(route, type, id) {

    const unknown = { 
      nameFirst: 'un',
      nameLast: 'known',
      getModelProp: () => { return 'unknown' },
      getAttr: () => { 
        return 'unknown'; 
      } 
    };

    const person = _.filter(peopleList, person => { 
      return person.id === id; 
    })[0];

    const family = _.filter(familyList, family => { 
      return family.getModelProp('husbandId') === person.id || family.getModelProp('wifeId') === person.id;
    })[0] || unknown;

    //console.log( family.renderDescendantTree(person, 3, familyList, peopleList) );

    const marriage = _.filter(family ? family.events : [], event => { 
      return event.tag === 'MARR'; 
    })[0] || unknown;


    const children = peopleList
          .filter(person => { 
            return _.filter(family ? family.children : [], child => { 
              return child.ref === person.id 
            }).length; 
          })
          .map(child => {
            return { 
              id: child.id, 
              sex: child.getAttr('sex'),
              name: child.getAttr('nameFirst') + child.getAttr('nameLast'),
              birthDate: child.getAttr('birthDate'),
              birthPlace: child.getAttr('birthPlace'),
              deathDate: child.getAttr('deathDate'),
              deathPlace: child.getAttr('deathPlace')
            };
          });

    console.log(children);

    let husband = family ? _.filter(peopleList, person => { 
      return person.id === family.getModelProp('husbandId'); 
    })[0] || unknown : unknown;
    
    if (husband.nameLast === 'known') {
      husband = person.getAttr('sex') === 'M' ? person : unknown;
    }

    let wife = family ? _.filter(peopleList, person => { 
      return person.id === family.getModelProp('wifeId'); 
    })[0] || unknown : unknown;

    if (wife.nameLast === 'known') {
      wife = person.getAttr('sex') === 'F' ? person : unknown;
    }
    
    let hParents = 'unknown';

    let momId = _.filter(familyList, family => {
      if (typeof husband.getModelProp('familiesBornInto')[0] !== 'undefined') {
        return family.id === husband.getModelProp('familiesBornInto')[0].ref;
      } else {
        return false;  
      }
    })[0];

    if (typeof momId !== 'undefined' && typeof momId.getModelProp('wifeId')) {
      console.log(momId);
      hParents = momId.getModelProp('wifeId');
    } 
   
    /* _.filter(peopleList, person => {
      return person.id === _.filter(familyList, family => {
        return family.id === husband.familiesBornInto[0].ref;
      })[0].wifeId;
    })[0].id;
    */

    const wifeParents = unknown;

    //config template
    
    const editTemplateScript = $('#edit-panel').html();

    const editTemplate = Handlebars.compile(editTemplateScript);

    const context = {
      husbandParents: hParents,
      husbandId: husband.id,
      husbandName: husband.nameFirst + husband.nameLast,
      husbandBirthDate: husband.getAttr('birthDate'),
      husbandBirthPlace: husband.getAttr('birthPlace'),
      husbandDeathDate: husband.getAttr('deathDate'),
      husbandDeathPlace: husband.getAttr('deathPlace'),
/*
      childId: child.id,
      childName: child.nameFirst + child.nameLast,
      childBirthDate: child.getAttr('birthDate'),
      childBirthPlace: child.getAttr('birthPlace'), 
      childDeathDate: child.getAttr('deathDate'),
      childDeathPlace: child.getAttr('deathPlace'),
      childSex: child.getAttr('sex'),*/

      children: children.slice(),

      marriageDate: family.getAttr('marriageDate'),
      marriagePlace: family.getAttr('marriagePlace'),
  
      wifeParents: wifeParents.id,
      wifeId: wife.id,
      wifeName: wife.nameFirst + wife.nameLast,
      wifeBirthDate: wife.getAttr('birthDate'),
      wifeBirthPlace: wife.getAttr('birthPlace'),
      wifeDeathDate: wife.getAttr('deathDate'),
      wifeDeathPlace: wife.getAttr('deathPlace'),
    };

    console.log('context', context);
/*
    const editCompiledHtml = editTemplate(context);

    $('#target').toggle();


    $('#target').html(editCompiledHtml);
    $('#target').fadeToggle();
*/
    renderMe('edit-panel', context, function () {


    // when someone changes text field

    $('.main-input').blur(function() {

      const id = $(this).data('id');

      const prop = $(this).data('prop');

      const person = _.filter(peopleList, person => { 
        return person.id === id; 
      })[0];

      const mcontext = {
        id: id, 
        prop: prop,
        person: person
      };

      const modalTemplateScript = $("#modal69").html();
      const modalTemplate = Handlebars.compile(modalTemplateScript);

      const modalCompiledHtml = modalTemplate(mcontext);

      $('#modal-target').html(modalCompiledHtml);

      $('#modal70').modal('show'); 

      $('#save-changes').click(function() {
        //person.setAttr(prop, );
        person.save('person', () => {
          $('#modal-target').html('<h4 class="text-center">Item successfully updated.</h4><img style="display:block; margin: auto;max-width:120px" src="success.gif">');
        });
      });

    });

    $('#print').click(() => { window.print(); });

//when someones looks at sources 

    $('.glyphicon-paperclip').click(function() {
      //debugger;
      const tag = $(this).data('tag');
      const id = $(this).data('id');
      const person = _.filter(peopleList, person => { return person.id === id; })[0];

      const sourceCitations = _.filter(person.events, event => { return event.tag === tag; })[0].sourceCitations[0];
      
      const source = _.filter(sourceList, source => { 
        return source.id === sourceCitations.ref;
      })[0];

      //debugger;

      const scontext = {
        ref: sourceCitations.ref,
        page: sourceCitations.page,
        title: source.title
      };

      const modalTemplateScript = $("#modal-sources").html();
      const modalTemplate = Handlebars.compile(modalTemplateScript);
      const modalCompiledHtml = modalTemplate(scontext);

      $('#modal-target').html(modalCompiledHtml);
      $('#modal70').modal('show');  
      // $('#source').modal('show'); 
    });

    $('.glyphicon-menu-right').click(() => { window.history.forward(); });
    $('.glyphicon-menu-left').click(() => { window.history.back(); });

    });

  },

  '/tree': function(route, type, id) {

    const person = _.filter(peopleList, person => { return person.id === id; })[0];
    
    const theTemplateScript = $("#edit-panel").html();
    const theTemplate = Handlebars.compile(theTemplateScript);

    const context = {
      husbandName: person.nameFirst + person.nameLast,
      husbandBirthDate: _.filter(person.events, event => { return event.tag === 'BIRT'; })[0].date,
      husbandBirthPlace: _.filter(person.events, event => { return event.tag === 'BIRT'; })[0].place,
    };

    const theCompiledHtml = theTemplate(context);

    console.log(theCompiledHtml, context);

    $('#target').html(theCompiledHtml);

    // call tree meths
  },

  '/contact': function(route, type, id) {
    renderMe();
  }
});



$(document).ready(function () {
  

  $.support.transition = true;

  $.material.init();

//use event delegation
// use data attributes for id and prop of each input. 
// save = open modal, spinner in modal, setmodelprop to new val, POST server
// if success, show success msg in modal. if fail, show fail msg in modal

/*
$(paperclip).click(function() {
  const id = this.data('id');
  const prop = this.data('prop');

  // render modal hbs with click handler on save button linked to id and prop
  // show modal
  // if save click, spinner in modal, setmodelprop, post server
});
*/


  $.ajaxSetup({
    contentType: 'application/json',
    timeout: 15000, 
    dataType: 'json',

    retryAfter:20000,
    error: ( function(){ func( param ) }, $.ajaxSetup().retryAfter ),
    beforeSend: function() {
    },
    complete: function(){
      //alert('success');
       //$('.spinner')[0].hide();
             console.log('complete');

    },
    success: function() {
      //alert('success');
      //$('.spinner')[0].hide();
      console.log('success!!!');
    }
  });

//url + '/api/all'

  $.get(url + '/api/all', function (data) {
    //debugger;
    console.log(data);
    $('#spinner').hide();



    if (data.people.length > 1) {
      _.forEach(data.people, person => { peopleList.push(new Person(person)); });
    }
    
    if (data.families.length > 1) {
      _.forEach(data.families, family => { familyList.push(new Family(family)); });
    }

    if (data.sources.length > 1) {
      _.forEach(data.sources, source => { sourceList.push(new Source(source)); });
      console.log(sourceList);
    }

    peopleList[478].descendantTree(6, familyList, peopleList);
    peopleList[234].descendantTree(6, familyList, peopleList);
    peopleList[0].descendantTree(6, familyList, peopleList);
    peopleList[2].descendantTree(6, familyList, peopleList);

    peopleList[0].ancestorTree(6, familyList, peopleList);

    peopleList[87].ancestorTree(6, familyList, peopleList);



    //debugger;
/*
    renderMe('edit-panel', {}, function () {
      // init search bar and typeahead
    var substringMatcher = function(strs) {
      return function findMatches(q, cb) {
        var matches, substrRegex;

        // an array that will be populated with substring matches
        matches = [];

        // regex used to determine if a string contains the substring `q`
        substrRegex = new RegExp(q, 'i');

        // iterate through the pool of strings and for any string that
        // contains the substring `q`, add it to the `matches` array
        $.each(strs, function(i, str) {
          if (substrRegex.test(str)) {
            matches.push(str);
          }
        });

        cb(matches);
      };
    };


    var states = _.map(peopleList, (person) => { return person.nameFirst + person.nameLast + ' ' + '(' + person.id + ')'; });

    $('#the-basics .typeahead').typeahead({
      hint: true,
      highlight: true,
      minLength: 1
    },
    {
      name: 'states',
      source: substringMatcher(states)
    });

    $('#the-basics').bind('typeahead:selected', function(obj, datum, name) {      
      const id = datum.slice(datum.length - 7, datum.length - 1);
      window.location.hash = '/' + id + 'edit';
    });


    console.log(peopleList, familyList);
    //peopleList[20].initDataBind( $('.person') );

    $.material.init();
    }); */

    debugger;

/*
    if (data.sources.length > 1) {
      _.forEach(data.sources, family => { sourceList.push(new Source(source)); });
    }
*/
/*
    var theTemplateScript = $("#edit-panel").html();
    var theTemplate = Handlebars.compile(theTemplateScript);
    var theCompiledHtml = theTemplate({});
    */

    //$('#target').html(theCompiledHtml);



    //people template

    /*

    const peopleContext = {
      people: _.map(peopleList, person => { return { name: person.nameFirst + person.nameLast, url: '#/' + person.id + 'edit' }; })
    };
/*


    const peopleContext = {};
    const peopleTemplateScript = $("#people").html();
    const peopleTemplate = Handlebars.compile(peopleTemplateScript);
    const peopleCompiledHtml = peopleTemplate(peopleContext);

    $('#target-people').html(peopleCompiledHtml);
*/

    

   
    });

  //init();

});