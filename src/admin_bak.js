'use strict';
const _ = require('lodash');
const unique = require('uniq');
const $ = require('jquery');
const jQuery = require('jquery');
require('typeahead.js');
window.$ = window.jQuery = jQuery;
require('bootstrap');

const url = 'http://127.0.0.1:5000';
// const url = 'http://agile-fortress-79861.herokuapp.com';

const panel = {
  head: "<div class='panel panel-primary col-md-4 col-lg-4'><div class='panel-heading'><div class='menu-buttons'><button type='button' data-toggle='modal' data-target='.bs-example-modal-lg' class='btn btn-primary btn-menu' aria-label='Left Align'><span class='glyphicon glyphicon-cog' aria-hidden='true'></span></button><button type='button' class='btn btn-primary btn-menu' aria-label='Left Align'><span class='glyphicon glyphicon-trash' aria-hidden='true'></span></button></div><span class='text-uppercase' id='box-id'>",
  body: "</span></div><div class='panel-body'>",
  tail: "</div></div>",
  div: "<div class='divider'><div class='row'>",
};

const modal = {
  head: "<h3>",
  body: "</h3><div class='input-group'><input type='text' class='form-control' placeholder='Edit...'><span class='input-group-btn'>",
  tail: "</span></div><!-- /input-group -->",
  div: "<div>"
};

class Person {
  constructor(obj) {
    this.id = obj.id;
    this.nameFirst = obj.nameFirst;
    this.nameLast = obj.nameLast;
    this.birth = this.getByTag(obj.events, 'BIRT');
    this.death = this.getByTag(obj.events, 'DEAT');
    this.emigration = this.getByTag(obj.events, 'EMIG');
    this.occupation = this.getByTag(obj.events, 'OCCU');
    this.address = obj.address;
    this.phone = obj.phon;
    this.eventsFacts = obj.events;
    this.sources = obj.events.sourceCitations;
  }
}

Person.prototype.renderModal = function(obj) {
  $('#modal-text').html(Person.prototype.renderAll.bind(obj, modal));
};

Person.prototype.getByTag = function(obj, str) {
  let result;
  const matching = _.filter(obj, (item) => { return item.tag === str; })[0];
  if (matching !== undefined) {
    result = _.reduce(matching, (prev, curr, i) => {
      (i === 'tag' || typeof curr === 'object') ? curr = '' : null;
      return prev + curr + '<br>';
    }, '');
  }     
  return result;
};

Person.prototype.renderAll = function(format) {
  let counter = 0;
  return (inner => { return _.reduce(this, (prev, curr, key) => {
    let result;
    if (curr === undefined) {
      result = prev + '';
    } else if (counter % 3 === 0 && counter !== 0) {
      result = prev + "</div><div class='row'>" + format.head + key + format.body + curr + format.tail;
    } else if (typeof curr === 'array') {
      let subResult = '';
      for(key in curr) {
        let obj = {};
        obj[curr[key].tag] = this.getByTag(curr, curr[key].tag);
        subResult += inner.bind(obj);
      }
      result = prev + subResult;
    } else {
      result = prev + format.head + key + format.body + curr + format.tail;
    }
    counter++;
    return result;
    }, format.div) + "</div>";
  })();
};

// append data to node, associate obj instance + id with node

Person.prototype.renderAppend = function($parentNode, format) {
  const $entry = $(this.renderAll(format));
  $parentNode.append($entry);
  $entry.attr("id", this.id);
  $entry.data("id", this.id);
  $entry.data("person", this);
};

function init(cb) {

  let activeBox = { person: undefined, title: undefined };
  let peopleList = [];
  let peopleObj = {};

  $.ajaxSetup({
    contentType: 'application/json; charset=utf-8',
    error: function () {
      //alert("fail");
      //callback getMyJson here in 5 seconds
      setTimeout(function () {
          init();
      }, 1000)
    }
  });

  $.get(url + '/api/people', (data) => {
    $('.names-container').html("<div class='input-group' id='the-basics'><input id='search' class='typeahead form-control' type='text' placeholder='Name'></div><div><button class='btn btn-default' id='search-button'>Search People</button></div>");

    _.forEach(data, (person) => { 
      const instPerson = new Person(person);
      instPerson.renderAppend($('.names-container'), panel); 
      peopleList.push(instPerson.nameFirst + ' ' + instPerson.nameLast);
      peopleObj[peopleList[peopleList.length - 1]] = peopleList[peopleList.length - 1].id;
    });
    
    // init search bar and typeahead
    var substringMatcher = function(strs) {
      return function findMatches(q, cb) {
        var matches, substrRegex;

        // an array that will be populated with substring matches
        matches = [];

        // regex used to determine if a string contains the substring `q`
        substrRegex = new RegExp(q, 'i');

        // iterate through the pool of strings and for any string that
        // contains the substring `q`, add it to the `matches` array
        $.each(strs, function(i, str) {
          if (substrRegex.test(str)) {
            matches.push(str);
          }
        });

        cb(matches);
      };
    };

    var states = peopleList;

    $('#the-basics .typeahead').typeahead({
      hint: true,
      highlight: true,
      minLength: 1
    },
    {
      name: 'states',
      source: substringMatcher(states)
    });

    // init click handlers for newly created elements

    $('#search-button').click(function() {
      let input = $('#search').val();

      console.log(_.filter(peopleList, person => { 
        return person.id === peopleObj[input]; 
      })[0]);
    });

    $('.panel-body').click(function() {
      const id = $(this).closest('.divider').data('id');
      $.get(url + '/api/families?husbandId=' + id, (data) => {
        console.log(data[0], $(this).closest('.divider').data('person'));
      });
    });

    $('.glyphicon-cog').click(function(event) {
      const person = $(this).closest('.divider').data("person");
      const title = $(this).parent().parent().parent().text();
      let fragment = {};

      $('#save-changes').removeClass('disabled');
      $('#save-changes').prop('disabled', false);

      fragment[title] = person[title];
      activeBox = { person: person, title: title };
      console.log("kkkkkkk",person);

      person.renderModal(fragment);
    });

    $('.modal-footer button').click(function() {
      console.log( $('#modal-text .input-group input').val() );
    });

    $('#save-changes').click(function(e) {
      const input = $('#modal-text .input-group input').val();
      let data = _.extend({}, activeBox);

      data.person[data.title] = input;
      data = JSON.stringify(data);

      $.post(url + '/api/person', data, 'json')
        .done((res) => {
          $('#modal-text').html('<div class="alert alert-success" role="alert">Item successfully updated!</div>');
          $('#save-changes').addClass('disabled');
          $('#save-changes').prop('disabled', true);
          console.log('success');
          // rerender
        });
    });

  });
}

$(document).ready(() => {
  $('.spinner').hide();
  jQuery.ajaxSetup({
  beforeSend: function() {
     $('.spinner').show();
  },
  complete: function(){
     $('.spinner').hide();
  },
  success: function() {
    console.log('success!!!');
  }
});
  init();
});