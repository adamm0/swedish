'use strict';
const _ = require('lodash');
const unique = require('uniq');
const $ = require('jquery');
const jQuery = require('jquery');
require('typeahead.js');
window.$ = window.jQuery = jQuery;
require('bootstrap');
const d3 = require('d3');
const swedishModels = require('./swedishModels.js');

const Person = swedishModels.Person;
const Family = swedishModels.Family;


//const url = 'http://127.0.0.1:5000';
const url = 'http://agile-fortress-79861.herokuapp.com';



function renderDescendantTree(person, generations) {
  let gen = generations;

  const ancestorTree = {
    id: person.id,
    _id: person.familyBornInto.ref,
    name: person.nameFirst + person.nameLast,
    children: []
  };

  function inner(tree, generations) {
    console.log("GENERATION: ", generations);
    if (generations > 0) {
      const family = _.filter(families, (fam) => { return fam.id === tree._id; })[0];

      const husband = _.filter(people, (person) => { 
        if(typeof person.familyStarted !== 'undefined' ) {
          if (person.familyStarted.ref === family.id) { console.log("father: ", person); }
          return person.familyStarted.ref === family.id; 
        } else{
          console.log('no family started for: ', person);
         return false; 
        }
      })[0];

      const wife = _.filter(people, (person) => { 
        if(typeof person.familyStarted !== 'undefined' ) {
          if (person.familyStarted.ref === family.id) { console.log("father: ", person); }
          return person.familyStarted.ref === family.id; 
        } else{
          console.log('no family started for: ', person);
         return false; 
        }
      })[1];

     // debugger;
    if (husband !== 'undefined') {
     // debugger;
      if (typeof husband.familyBornInto !== 'undefined') {
       // debugger;
        tree.children.push({ id: husband.id, _id: husband.familyBornInto.ref, name: husband.nameFirst + husband.nameLast, birth: husband.birth, children: [] }); 
      if (typeof wife.familyBornInto !== 'undefined') {
        tree.children.push( _.extend(wife, { id: wife.id, _id: wife.familyBornInto.ref, name: wife.nameFirst + wife.nameLast, birth: wife.birth, children: [] }));

      } 

        _.forEach(tree.children, (child) => { inner(child, generations); });

      } else{ console.log('no family born into listed for: ', husband); }
    } else { console.log('could not find husband'); }
      generations -= 1;
    } else {
      return;
    }
  }

  inner(ancestorTree, gen);
  return ancestorTree;
}

let id = 0;
let name = 0;

let people = [];
let families = [];



Family.prototype.renderTree = function(people, families, generations) {
  const rootNode = _.extend({ depth1: 0 }, this);

  function inner(node) {
    if (node.hasOwnProperty('husband')) {
      if (node.children.length > 0) {
        node.children = _.map(node.children, child => {
          if (typeof child === 'object') {
            return child.hasOwnProperty('familyStarted') === false ? child : _.filter(families, family => {
              return family.id === child.familyStarted.ref;
            })[0];
          } else {
            //debugger;
            return { parent: node.name, name: name++, id: id++ };
          }
        });

        node.children = _.map(node.children, child => { return _.extend(child, { depth1: node.depth1 + 1 }); });
        
        if (node.depth1 < generations) {
          _.forEach(node.children, child => { inner(child); });
        } else {
          return 1;
        }
      }
    }
  };  
  inner(rootNode);
  console.log("###", JSON.stringify(rootNode) );
  return rootNode;
};


Person.prototype.getByTag = function(obj, str) {
  let result;
  const matching = _.filter(obj, (item) => { return item.tag === str; })[0];
  if (matching !== undefined) {
    result = _.reduce(matching, (prev, curr, i) => {
      (i === 'tag' || typeof curr === 'object') ? curr = '' : null;
      return prev + curr ;//+ '<br>';
    }, '');
  }     
  return result;
};

// append data to node, associate obj instance + id with node

Person.prototype.renderAppend = function($parentNode, format) {
  const $entry = $(this.renderAll(format));
  $parentNode.append($entry);
  $entry.attr("id", this.id);
  $entry.data("id", this.id);
  $entry.data("person", this);
};

function init(cb) {



  $.ajaxSetup({
    contentType: 'application/json; charset=utf-8',
    timeout: 3000, 
    retryAfter:4000,
    error: ( function(){ func( param ) }, $.ajaxSetup().retryAfter )
  });

  $.get(url + '/api/people', (data) => {
    console.log(people);



function renderDescendantTree(person, generations) {
  let gen = generations;

  const ancestorTree = {
    _id: person.familyBornInto.ref,
    name: person.nameFirst + person.nameLast,
    children: []
  };

  function inner(tree, generations) {
    console.log("GENERATION: ", generations);
    if (generations > 0) {
      const family = _.filter(families, (fam) => { return fam.id === tree._id; })[0];

      const husband = _.filter(people, (person) => { 
        if(typeof person.familyStarted !== 'undefined' ) {
          if (person.familyStarted.ref === family.id) { console.log("father: ", person); }
          return person.familyStarted.ref === family.id; 
        } else{
          console.log('no family started for: ', person);
         return false; 
        }
      })[0];

      const wife = _.filter(people, (person) => { 
        if(typeof person.familyStarted !== 'undefined' ) {
          if (person.familyStarted.ref === family.id) { console.log("father: ", person); }
          return person.familyStarted.ref === family.id; 
        } else{
          console.log('no family started for: ', person);
         return false; 
        }
      })[1];

     // debugger;
    if (husband !== 'undefined') {
     // debugger;
      if (typeof husband.familyBornInto !== 'undefined') {
       // debugger;
        tree.children.push({ _id: husband.familyBornInto.ref, name: husband.nameFirst + husband.nameLast, birth: husband.birth, children: [] }); 
      if (typeof wife.familyBornInto !== 'undefined') {
        tree.children.push( _.extend(wife, { _id: wife.familyBornInto.ref, name: wife.nameFirst + wife.nameLast, birth: wife.birth, children: [] }));

      } 

        _.forEach(tree.children, (child) => { inner(child, generations); });

      } else{ console.log('no family born into listed for: ', husband); }
    } else { console.log('could not find husband'); }
      generations -= 1;
    } else {
      return;
    }
  }

  inner(ancestorTree, gen);
  return ancestorTree;
}

/*

get people
get families

fn inner (tree, generations)
  if generations
    fam = filter families 
      return family.id === person.famc[0].ref

    tree.children.push(
      filter people 
        return person.id === fam.husbandRefs[0].ref,
      wife
    );
    
    generations--
    each tree.children
      inner(child)

  else 
    return

inner (tree, generations)

return tree

*/

    // $('.names-container').html("<div class='input-group' id='the-basics'><input class='typeahead form-control' type='text' placeholder='Name'></div><div><button class='btn btn-default' id='search-button'>Search People</button></div>");

    _.forEach(data, (person) => { 
      const instPerson = new Person(person);
      people.push(instPerson);
    });



        // init search bar and typeahead
    var substringMatcher = function(strs) {
      return function findMatches(q, cb) {
        var matches, substrRegex;

        // an array that will be populated with substring matches
        matches = [];

        // regex used to determine if a string contains the substring `q`
        substrRegex = new RegExp(q, 'i');

        // iterate through the pool of strings and for any string that
        // contains the substring `q`, add it to the `matches` array
        $.each(strs, function(i, str) {
          if (substrRegex.test(str)) {
            matches.push(str);
          }
        });

        cb(matches);
      };
    };

    var states = _.map(people, (person) => { return person.nameFirst + person.nameLast + ' ' + '(' + person.id + ')'; });

    $('#the-basics .typeahead').typeahead({
      hint: true,
      highlight: true,
      minLength: 1
    },
    {
      name: 'states',
      source: substringMatcher(states)
    });




          $.get(url + '/api/families', (data1) => {
            console.log(data1);
            _.forEach(data1, family => {
              const fam = new Family(family, people);
              families.push(fam);
            });
            console.log(families);

            //debugger;
            //console.log("!", families[5].renderTree(people, families, 1));


            //new
            const person = _.filter(people, (person) => { return person.id === 'I00060'; })[0];
            console.log("person target: ", person);
            console.log("###$$$#",renderDescendantTree(person, 3));

    // d3 -->

var w = 2000,
    h = 2000, // 2000
    i = 0,
    duration = 500,
    root;


function mouseover(d) {
                div.transition()
                .duration(300)
                .style("opacity", 1)

                .text("Info about: " + d.name + ' ' + d.birth + ' ' + d.death + ' ' + d.occupation) 
                .style("left", (d3.event.pageX ) + "px")
                .style("top", (d3.event.pageY) + "px");
            }

            function mousemove(d) {
              
                div
                .text("Info about: " + d.name + ' ' + d.birth + ' ' + d.death + ' ' + d.occupation) 
                .style("opacity", 1)

                .style("left", (d3.event.pageX ) + "px")
                .style("top", (d3.event.pageY) + "px");

            }

            function mouseout() {
                div.transition()
                .duration(3000)
                .style("opacity", 1e-6);
            }

     var div = d3.select("body").append("div")
            .attr("class", "tooltip")
            .style("opacity", 1e-6);

var tree = d3.layout.tree()
    .separation(function(a, b) { return ((a.parent == root) && (b.parent == root)) ? 8 : 4; })

    //.size([2000, 2000]);
    .size([h, w - 160]);

     tree.nodeSize = function(x) {
    if (!arguments.length) return nodeSize ? size : null;
    nodeSize = (size = x) != null;
    return tree;
  };

var diagonal = d3.svg.diagonal()
    .projection(function(d) { return [d.y, d.x]; });

var vis = d3.select(".tree").append("svg:svg")
    .attr("width", w)
    .attr("height", h)
    .append("svg:g")
    .attr("transform", "translate(40,0)");

d3.json("people.json", function(json) {

  const person = _.filter(people, (person) => { return person.id === 'I00061'; })[0];
  //console.log("person target: ", person);
  json = renderDescendantTree(person, 3);

  //json = families[1].renderTree(people, families, 3); //remove
  console.log("^", json);
  json.x0 = 800;
  json.y0 = 0;
  update(root = json);
});

function update(source) {

  // Compute the new tree layout.
  var nodes = tree.nodes(root).reverse();
 console.log(nodes)
  // Update the nodes…
    var node = vis.selectAll("g.node")
      .data(nodes, function(d) { return d.id || (d.id = ++i); });

  var nodeEnter = node.enter().append("svg:g")
      .attr("class", "node")
      .attr('width', '75')
      .attr("transform", function(d) { return "translate(" + source.y0 + "," + source.x0 + ")"; })
      .on("click", function(){ 
        console.log(this.__data__._id);
        debugger;
        console.log("&&&&&&", familyId);
        familyId = _.filter(people, person => { return person.id === this.__data._id; })[0].familyStarted.ref;
        window.location.href = "/" + familyId;
      });

      //.style("opacity", 1e-6);
 
  // Enter any new nodes at the parent's previous position.
 
    nodeEnter.append("svg:rect")
      //.attr("class", "node")
      //.attr("cx", function(d) { return source.x0; })
      //.attr("cy", function(d) { return source.y0; })
      //.attr("y", -15)
      .style("fill", function(d) { return d._children ? "lightsteelblue" : "#fff"; })
      .attr('class', 'box')
      .attr('pointer-events', 'all')
      .on("mousemove", mousemove)
      .on("mouseout", mouseout)
      .on("mouseover", mouseover);


      
      //.on("click", click)
      //.on("mouseover", function(){ $('.panel-info').css('visibility', 'visible'); });

  nodeEnter.append("svg:text")
        //.attr("x", function(d) { return d._children ? -8 : 8; })
        //.attr('x', -70)
    //.attr("y", -10)
        //.attr("fill","#ccc")
        //.attr("transform", function(d) { return "translate(" + d.y + "," + d.x + ")"; })
 
        .text(function(d) { console.dir(d);return d.name + ' ' + d.birth + d.id; });

  var insertLinebreaks = function (t, d, width) {
    var el = d3.select(t);
    var p = d3.select(t.parentNode);
    p.append("foreignObject")
       // .attr('id', d._id)
        .attr('x', -70)
        .attr('y', -5)
        .attr("width", width)
        .attr("height", 200)
      .append("xhtml:p")
        .attr('style','word-wrap: break-word; text-align:center; font-size:11px')
        .html(d);

    el.remove();

  };

  d3.select('.tree')
    .selectAll('text')
        .each(function(d,i){ 
          insertLinebreaks(this, d.name + d.birth, 150 );
        });


  // Transition nodes to their new position.
  nodeEnter.transition()
    .duration(duration)
    .attr("transform", function(d) { return "translate(" + d.y + "," + d.x + ")"; })
        .style("opacity", 1)
      .select("rect")
      .attr('x', -70)
      .attr('y', -15)
      //.attr("cx", function(d) { return d.x; })
    //.attr("cy", function(d) { return d.y; })
        .style("fill", "lightsteelblue");
      
    node.transition()
      .duration(duration)
      .attr("transform", function(d) { return "translate(" + d.y + "," + d.x + ")"; })
      .style("opacity", 1);
    
  node.exit().transition()
      .duration(duration)
      .attr("transform", function(d) { return "translate(" + source.y + "," + source.x + ")"; })
      .style("opacity", 1e-6)
      .remove();
/*
  var nodeTransition = node.transition()
    .duration(duration);
  
  nodeTransition.select("circle")
      .attr("cx", function(d) { return d.y; })
      .attr("cy", function(d) { return d.x; })
      .style("fill", function(d) { return d._children ? "lightsteelblue" : "#fff"; });
  
  nodeTransition.select("text")
      .attr("dx", function(d) { return d._children ? -8 : 8; })
    .attr("dy", 3)
      .style("fill", function(d) { return d._children ? "lightsteelblue" : "#5babfc"; });

  // Transition exiting nodes to the parent's new position.
  var nodeExit = node.exit();
  
  nodeExit.select("circle").transition()
      .duration(duration)
      .attr("cx", function(d) { return source.y; })
      .attr("cy", function(d) { return source.x; })
      .remove();
  
  nodeExit.select("text").transition()
      .duration(duration)
      .remove();
*/
  // Update the links…
  var link = vis.selectAll("path.link")
      .data(tree.links(nodes), function(d) { return d.target.id; });

  // Enter any new links at the parent's previous position.
  link.enter().insert("svg:path", "g")
      .attr("class", "link")
      .attr("d", function(d) {
        var o = {x: source.x0, y: source.y0};
        return diagonal({source: o, target: o});
      })
    .transition()
      .duration(duration)
      .attr("d", diagonal);
  // Transition links to their new position.
  link.transition()
      .duration(duration)
      .attr("d", diagonal);

  // Transition exiting nodes to the parent's new position.
  link.exit().transition()
      .duration(duration)
      .attr("d", function(d) {
        var o = {x: source.x, y: source.y};
        return diagonal({source: o, target: o});
      })
      .remove();

  // Stash the old positions for transition.
  nodes.forEach(function(d) {
    d.x0 = d.x;
    d.y0 = d.y;
  });
}

// Toggle children on click.
function click(d) {
  if (d.children) {
    d._children = d.children;
    d.children = null;
  } else {
    d.children = d._children;
    d._children = null;
  }
  update(d);
}




d3.select(self.frameElement).style("height", "2000px");
    // /d3 -->
  });

  });
}

$(document).ready(function() {
  $('.spinner').hide();
  jQuery.ajaxSetup({
    beforeSend: function() {
       $('.spinner').show();
    },
    complete: function(){
       $('.spinner').hide();
    },
    success: function() {
      console.log('success!!!');
    }
  });
  init();

  $('button#search-button.btn.btn-default').click(function() {

    $('.tree').empty();
    let id = $('input#search.typeahead.form-control.tt-input').val();
    $('.tree').append("<h3>" + id.slice(0, id.length - 9) + "'s" + " Ancestors" + "</h3>");

    id = id.slice(id.length - 7, id.length - 1);
    //alert(id);
    const person = _.filter(people, (person) => { return person.id === id; })[0];
    console.log("!!!! %%% %% ^^", person);

    var w = 1500,
    h = 1200, // 2000
    i = 0,
    duration = 500,
    root;


function mouseover() {
                div.transition()
                .duration(300)
                .style("opacity", 1)

                                .text("Info about " + d.name + ' ' + d.birth + ' ' + d.death + ' ' + d.occupation) 

                .style("left", (d3.event.pageX ) + "px")
                .style("top", (d3.event.pageY) + "px");
            }

            function mousemove(d) {
              
                div
                .text("Info about: " + d.name + ' ' + (d.birth !== undefined ? d.birth : 'unknown') + ' ' + (d.death !== undefined ? ' - ' + d.death : '') + ' ' + (d.occupation !== undefined ? 'Occupation: ' + d.occupation : '')) 

                .style("left", (d3.event.pageX ) + "px")
                .style("top", (d3.event.pageY) + "px");

            }

            function mouseout() {
                div.transition()
                .duration(3000)
                .style("opacity", 1e-6);
            }

     var div = d3.select("body").append("div")
            .attr("class", "tooltip")
            .style("opacity", 1e-6);

var tree = d3.layout.tree()
    .separation(function(a, b) { return ((a.parent == root) && (b.parent == root)) ? 3 : 1; })

    //.size([2000, 2000]);
    .size([h, w - 160]);

     tree.nodeSize = function(x) {
    if (!arguments.length) return nodeSize ? size : null;
    nodeSize = (size = x) != null;
    return tree;
  };

var diagonal = d3.svg.diagonal()
    .projection(function(d) { return [d.y, d.x]; });

var vis = d3.select(".tree").append("svg:svg")
    .attr("width", w)
    .attr("height", h)
    .append("svg:g")
    .attr("transform", "translate(40,0)");

d3.json("people.json", function(json) {

  //console.log("person target: ", person);
  json = renderDescendantTree(person, 3);

  //json = families[1].renderTree(people, families, 3); //remove
  console.log("^", json);
  json.x0 = 800;
  json.y0 = 0;
  update(root = json);
});

function update(source) {
  // Compute the new tree layout.
  var nodes = tree.nodes(root).reverse();
 console.log(nodes)
  // Update the nodes…
    var node = vis.selectAll("g.node")
      .data(nodes, function(d) { return d.id || (d.id = ++i); });

  var nodeEnter = node.enter().append("svg:g")
      .attr("class", "node")
      .attr('width', '75')
      .attr("transform", function(d) { return "translate(" + source.y0 + "," + source.x0 + ")"; })
             .on("click", function(){ 
              console.log("%^%^%^%^bitthc",$(this)[0].lastChild.textContent);
              console.dir(this.__data__);
              debugger;


        familyId = _.filter(people, person => { return person.id === this.__data._id; })[0].familyStarted.ref;
        window.location.href = "/" + familyId;

      })
      .style("opacity", 1e-6);
 
  // Enter any new nodes at the parent's previous position.
 
    nodeEnter.append("svg:rect")
      //.attr("class", "node")
      //.attr("cx", function(d) { return source.x0; })
      //.attr("cy", function(d) { return source.y0; })
      //.attr("y", -15)
      .style("fill", function(d) { return d._children ? "lightsteelblue" : "#fff"; })
      .attr('class', 'box')
      .attr('pointer-events', 'all')
      .on("mousemove", mousemove)

      .on("mouseover", mouseover);


      
      //.on("click", click)
      //.on("mouseover", function(){ $('.panel-info').css('visibility', 'visible'); });

  nodeEnter.append("svg:text")
        //.attr("x", function(d) { return d._children ? -8 : 8; })
        //.attr('x', -70)
    //.attr("y", -10)
        //.attr("fill","#ccc")
        //.attr("transform", function(d) { return "translate(" + d.y + "," + d.x + ")"; })
 
        .text(function(d) { return d.name + ' ' + d.birth + d.id; });

  var insertLinebreaks = function (t, d, width) {
    var el = d3.select(t);
    var p = d3.select(t.parentNode);
    p.append("foreignObject")
        .attr('x', -70)
        .attr('y', -5)
        .attr("width", width)
        .attr("height", 200)
      .append("xhtml:p")
        .attr('style','word-wrap: break-word; text-align:center; font-size:11px')
        .html(d);

    el.remove();

  };

  d3.select('.tree')
    .selectAll('text')
        .each(function(d,i){ 
          insertLinebreaks(this, d.name + d.birth + d.id, 150 );
        });


  // Transition nodes to their new position.
  nodeEnter.transition()
    .duration(duration)
    .attr("transform", function(d) { return "translate(" + d.y + "," + d.x + ")"; })
        .style("opacity", 1)
      .select("rect")
      .attr('x', -70)
      .attr('y', -15)
      //.attr("cx", function(d) { return d.x; })
    //.attr("cy", function(d) { return d.y; })
        .style("fill", "lightsteelblue");
      
    node.transition()
      .duration(duration)
      .attr("transform", function(d) { return "translate(" + d.y + "," + d.x + ")"; })
      .style("opacity", 1);
    
  node.exit().transition()
      .duration(duration)
      .attr("transform", function(d) { return "translate(" + source.y + "," + source.x + ")"; })
      .style("opacity", 1e-6)
      .remove();
/*
  var nodeTransition = node.transition()
    .duration(duration);
  
  nodeTransition.select("circle")
      .attr("cx", function(d) { return d.y; })
      .attr("cy", function(d) { return d.x; })
      .style("fill", function(d) { return d._children ? "lightsteelblue" : "#fff"; });
  
  nodeTransition.select("text")
      .attr("dx", function(d) { return d._children ? -8 : 8; })
    .attr("dy", 3)
      .style("fill", function(d) { return d._children ? "lightsteelblue" : "#5babfc"; });

  // Transition exiting nodes to the parent's new position.
  var nodeExit = node.exit();
  
  nodeExit.select("circle").transition()
      .duration(duration)
      .attr("cx", function(d) { return source.y; })
      .attr("cy", function(d) { return source.x; })
      .remove();
  
  nodeExit.select("text").transition()
      .duration(duration)
      .remove();
*/
  // Update the links…
  var link = vis.selectAll("path.link")
      .data(tree.links(nodes), function(d) { return d.target.id; });

  // Enter any new links at the parent's previous position.
  link.enter().insert("svg:path", "g")
      .attr("class", "link")
      .attr("d", function(d) {
        var o = {x: source.x0, y: source.y0};
        return diagonal({source: o, target: o});
      })
    .transition()
      .duration(duration)
      .attr("d", diagonal);
  // Transition links to their new position.
  link.transition()
      .duration(duration)
      .attr("d", diagonal);

  // Transition exiting nodes to the parent's new position.
  link.exit().transition()
      .duration(duration)
      .attr("d", function(d) {
        var o = {x: source.x, y: source.y};
        return diagonal({source: o, target: o});
      })
      .remove();

  // Stash the old positions for transition.
  nodes.forEach(function(d) {
    d.x0 = d.x;
    d.y0 = d.y;
  });
}

// Toggle children on click.
function click(d) {
  if (d.children) {
    d._children = d.children;
    d.children = null;
  } else {
    d.children = d._children;
    d._children = null;
  }
  update(d);
}



d3.select(self.frameElement).style("height", "2000px");
    // /d3 -->
  });

$('rect').click(function(){
  alert('lkj');
});

  });


