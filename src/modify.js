'use strict';
const _ = require('lodash');
const $ = require('jquery');
const jQuery = require('jquery');
window.$ = window.jQuery = jQuery;
require('bootstrap');
require('bootstrap-material-design/dist/js/material.js');
require('proxy-polyfill');

const swedishApp = require('./swedishRouter.js');
const swedishModels = require('./swedishModels.js');

global.app = new swedishApp.Router();

const Person = swedishModels.Person;
const Family = swedishModels.Family;
const Note = swedishModels.Note;
const Source = swedishModels.Source;

global.url = 'http://agile-fortress-79861.herokuapp.com';
//'http://127.0.0.1:5000';
//const url = 'http://agile-fortress-79861.herokuapp.com';

let peopleList = [];
let familyList = [];

app.config['target'] = $('.starter-template');

app.routes({

  '/edit': function(route, type, id) {
    const person = _.filter(peopleList, person => { return person.id === id; })[0];
    const wife = {};
    const cousin = {}; 
    //const children = person.children;
 
    let bindings = [
      [ person, '.dog' ],
      [ person, '.wife' ],
      [ cousin, '#crazy-cousin' ]
    ];

    _.forEach(person.children, (child, i) => { bindings.push( [ child, 'child' + i ] ); });

    app.renderView(route, bindings, success);

    function success() {
     _.forEach(children, (child, i) => { $('table').append("<tr class='" + child + i + "'>" ); });
    }

  },

  '/about': function(route, type, id) {
    
  },

  '/contact': function(route, type, id) {
    window.location = 'http://www.yahoo.com';
  }
});

function init(cb) {


  $.get(url + '/api/all', (data) => {
    _.forEach(data.people, (person) => { peopleList.push(new Person(person)); });
    //_.forEach(data, (family) => { familyList.push(new Family(family, peopleList)); });

    console.log(peopleList);
    peopleList[20].initDataBind( $('.person') );

    });



}

$(document).ready(() => {
  //$.support.transition = true;

  $.material.init();

  $('.main-input').blur(() => { $('#myModal').modal('show'); });
  $('#print').click(() => { window.print(); });
  $('.glyphicon-paperclip').click(() => { $('#source').modal('show'); });
  $('.glyphicon-menu-right').click(() => { window.history.forward(); });
  $('.glyphicon-menu-left').click(() => { window.history.back(); });

  $('.spinner').hide();

  $.ajaxSetup({
    contentType: 'application/json',
    timeout: 3000, 
    retryAfter:4000,
    error: ( function(){ func( param ) }, $.ajaxSetup().retryAfter ),
    beforeSend: function() {
    },
    complete: function(){
      alert('success');
       //$('.spinner')[0].hide();
             console.log('complete');

    },
    success: function() {
      alert('success');
      //$('.spinner')[0].hide();
      console.log('success!!!');
    }
  });

  $.get(url + '/api/all', (data) => {
    if(data.people.length > 1) {

    _.forEach(data.people, (person) => { peopleList.push(new Person(person)); });
    //_.forEach(data, (family) => { familyList.push(new Family(family, peopleList)); });
}
    console.log(peopleList);
    peopleList[20].initDataBind( $('.person') );

    });


  //init();

});