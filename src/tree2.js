'use strict';
const _ = require('lodash');
const unique = require('uniq');
const $ = require('jquery');
const jQuery = require('jquery');
require('typeahead.js');
window.$ = window.jQuery = jQuery;
require('bootstrap');
const d3 = require('d3');


const url = 'http://127.0.0.1:5000';
//const url = 'http://agile-fortress-79861.herokuapp.com';

let x = 0;
let id = 0;

class Family {
  constructor(family, people) {
    this.depth1;
    this.id = id+=1; //family.id;
    this.husband = _.filter(people, person => { return person.id === family.husbandId; })[0];
    this.wife = _.filter(people, person => { return person.id === family.wifeId; })[0];
    this.name = 'name' + x++ + '';
    this.events = family.events;
    this.children = _.map(family.childIds, id => { 
      return _.filter(people, person => { 
        return person.id === id; 
      })[0]; 
    });
  }
}

Family.prototype.renderTree = function(people, families, generations) {
  const rootNode = _.extend({ depth1: 0, parent: 'null' }, this);

  function inner(node) {
    if (node.children.length > 0) {
      node.children = _.map(node.children, child => {
        if (typeof child !== 'undefined') {
        return child.hasOwnProperty('familyStarted') === false ? child : _.filter(families, family => {
          return family.id === child.familyStarted.ref;
        })[0];
      }
      });
      node.children = _.map(node.children, child => { return _.extend(child, { depth1: node.depth1 + 1, parent: node.name }); });
      if (node.depth1 < generations) {
        _.forEach(node.children, child => { node.hasOwnProperty('children') ? inner(child) : null; });
      } else {
        return 1;
      }
    }
  }
  console.log("*",rootNode);
  inner(rootNode);
/*
  function trim(rootNode) {
    _.forEach(Object.getOwnPropertyNames(rootNode), propName => {
      if (propName === 'children' || propName === 'name' || propName === 'parent') {
          null; 
      } else { 
        console.log("deleting: ", propName); 
        delete rootNode[propName];
      }
      if (rootNode.hasOwnProperty('children')) {
        if(rootNode.children !== 'undefined')
          rootNode.children.length > 0 ? _.forEach(rootNode.children, child => { trim(child); }) : null; 
      }
    });
  }
  console.log("normal: ",rootNode);
  trim(rootNode);
  console.log('trim: ', rootNode);
  */
  console.log("!!!!!!", JSON.stringify(rootNode));
  return JSON.stringify(rootNode);
};

class Person {
  constructor(obj) {
    this.id = id += 1;// obj.id;
    this.name = "person" + x++ + "";
    this.nameFirst = obj.nameFirst;
    this.nameLast = obj.nameLast;
    this.birth = this.getByTag(obj.events, 'BIRT');
    this.death = this.getByTag(obj.events, 'DEAT');
    this.emigration = this.getByTag(obj.events, 'EMIG');
    this.occupation = this.getByTag(obj.events, 'OCCU');
    this.address = obj.address;
    this.phone = obj.phon;
    this.eventsFacts = obj.events;
    this.sources = obj.events.sourceCitations;

    this.children = [];
    this.depth1;
    this.familyBornInto = obj.famc[0];
    this.familyStarted = obj.fams[0];
  }
}

Person.prototype.getByTag = function(obj, str) {
  let result;
  const matching = _.filter(obj, (item) => { return item.tag === str; })[0];
  if (matching !== undefined) {
    result = _.reduce(matching, (prev, curr, i) => {
      (i === 'tag' || typeof curr === 'object') ? curr = '' : null;
      return prev + curr + '<br>';
    }, '');
  }     
  return result;
};

Person.prototype.renderAll = function(format) {
  let counter = 0;
  return (inner => { return _.reduce(this, (prev, curr, key) => {
    let result;
    if (curr === undefined) {
      result = prev + '';
    } else if (counter % 3 === 0 && counter !== 0) {
      result = prev + "</div><div class='row'>" + format.head + key + format.body + curr + format.tail;
    } else if (typeof curr === 'array') {
      let subResult = '';
      for(key in curr) {
        let obj = {};
        obj[curr[key].tag] = this.getByTag(curr, curr[key].tag);
        subResult += inner.bind(obj);
      }
      result = prev + subResult;
    } else {
      result = prev + format.head + key + format.body + curr + format.tail;
    }
    counter++;
    return result;
    }, format.div) + "</div>";
  })();
};

// append data to node, associate obj instance + id with node

Person.prototype.renderAppend = function($parentNode, format) {
  const $entry = $(this.renderAll(format));
  $parentNode.append($entry);
  $entry.attr("id", this.id);
  $entry.data("id", this.id);
  $entry.data("person", this);
};

function init(cb) {

  let people = [];
  let families = [];

  $.ajaxSetup({
    contentType: 'application/json; charset=utf-8',
    error: function () {
      setTimeout(function () {
          init();
      }, 1000);
    }
  });

  $.get(url + '/api/people', (data) => {


    $('.names-container').html("<div class='input-group' id='the-basics'><input class='typeahead form-control' type='text' placeholder='Name'></div><div><button class='btn btn-default' id='search-button'>Search People</button></div>");

    _.forEach(data, (person) => { 
      const instPerson = new Person(person);
      people.push(instPerson);
    });

          $.get(url + '/api/families', (data1) => {
            console.log(data1);
            _.forEach(data1, family => {
              const fam = new Family(family, people);
              families.push(fam);
            });
            console.log(families);

            //debugger;
            console.log("!", families[2].renderTree(people, families, 3));

    // d3 -->
    var diameter = 960;

    var tree = d3.layout.tree()
        .size([360, diameter / 2 - 120])
        .separation(function(a, b) { console.log("$",a,b);return (a.parent == b.parent ? 1 : 2) / a.depth; });

    var diagonal = d3.svg.diagonal.radial()
        .projection(function(d) { return [d.y, d.x / 180 * Math.PI]; });

    var svg = d3.select(".names-container").append("svg")
        .attr("width", diameter)
        .attr("height", diameter - 150)
      .append("g")
        .attr("transform", "translate(" + diameter / 2 + "," + diameter / 2 + ")");

    d3.json('people-bak-sample.json', function(error, root) {
      if (error) throw error;
      var nodes = tree.nodes(families[5].renderTree(people, families, 3)),
          links = tree.links(nodes);

      var link = svg.selectAll(".link")
          .data(links)
        .enter().append("path")
          .attr("class", "link")
          .attr("d", diagonal);

      var node = svg.selectAll(".node")
          .data(nodes)
        .enter().append("g")
          .attr("class", "node")
          .attr("transform", function(d) { return "rotate(" + (d.x - 90) + ")translate(" + d.y + ")"; });

      node.append("circle")
          .attr("r", 4.5);

      node.append("text")
          .attr("dy", ".31em")
          .attr("text-anchor", function(d) { return d.x < 180 ? "start" : "end"; })
          .attr("transform", function(d) { return d.x < 180 ? "translate(8)" : "rotate(180)translate(-8)"; })
          .text(function(d) { return d.name; });
    });

    d3.select(self.frameElement).style("height", diameter - 150 + "px");
    // /d3 -->
  });

  });
}

$(document).ready(() => {
  $('.spinner').hide();
  jQuery.ajaxSetup({
  beforeSend: function() {
     $('.spinner').show();
  },
  complete: function(){
     $('.spinner').hide();
  },
  success: function() {
    console.log('success!!!');
  }
});
  init();
});