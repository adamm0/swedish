#!/bin/bash

DATE="$1"
echo "*** Restoring database dump from $DATE ***" 
mongorestore --drop --db testdb backup/"$DATE"/testdb