#!/bin/bash

DATE="$(date)" 
echo "*** Creating database backup for $DATE ***" 
mongodump --out backup/"$DATE" 
